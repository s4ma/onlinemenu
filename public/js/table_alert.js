
///shoppingcart/table_alert

getAvaliableStores();

$(document).ready(function () {
    setInterval(() => {

        getAvaliableStores();

    }, 5000)
});


function getAvaliableStores(){
    $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/shoppingcart/table_alert',
        method: "POST",
        dataType: 'json',
        data: {
            '_token': $("meta[name='csrf-token']").attr("content"),
            store_id: parseInt($(".store_id").attr('data-id'))
        }
    }).done(resp => {
        resp.forEach(element => {
            if(element.in_use == 1){
                
                 $(`div[data-name="table_${element.table_name}"]`).css("border", "4px solid #fb7f40"); 
            }else{
                $(`div[data-name="table_${element.table_name}"]`).css('border','2px dashed #001354');
            }
        });
    });
}