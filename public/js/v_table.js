var first_table = document.getElementById('first_table');
var second_table = document.getElementById('second_table');
var cambio = true;



const FILLMAPS = new Promise((resolve, reject)=>{
    const mapsTables = fillMaps(ind_orders, map_orders, cart);
    if(mapsTables){
        resolve(mapsTables);
    }else{
        reject(false);
    }
});

FILLMAPS.then(res=>{
    ////console.log(res);
    let map_primera_tabla = res[0];
    let map_segunda_tabla = res[1];


    var update_tables = setInterval(()=>{
        if(cambio){
            ////console.log(cambio);

            updateTableOrders(map_primera_tabla,map_segunda_tabla);
            updateTableCart(map_segunda_tabla);
            cambio = false;
        }
    }, 1000);


}, error=>{
    //console.log(error);
});




function assign_functionDeleteOrder(map_primera_tabla) {
    let delete_buttons = document.querySelectorAll('.btn_delete');
    delete_buttons.forEach(el => {
        
        let i = el.getAttribute('data-id');
        i = parseInt(i);
        el.onclick = ()=>{DB_delete_order(map_primera_tabla, i )}
        

    });
} //YAAA

function assign_FunctionAddCart(map_primera_tabla, map_segunda_tabla) {
    let add_to_cart_buttons = document.querySelectorAll('.add_to_cart');

    add_to_cart_buttons.forEach(button => {

        let i = button.getAttribute('data-id');
        i = parseInt(i);
        
        button.onclick = ()=>{add_to_cart(i, map_primera_tabla, map_segunda_tabla)}

        

    });
} //YAAA

function assign_FunctionRejectOrder(map_primera_tabla){

    let btn_rejects = document.querySelectorAll('.btn_reject');

    btn_rejects.forEach(element => {
        let i = parseInt(element.getAttribute('data-id'));

        element.onclick = ()=>{reject_order(map_primera_tabla, i)}
        
    });

} //YAAAA




function fillMaps(ind_orders, map_orders, cart) {

    ind_orders.clear();
    let ii = parseInt(cart.size);

    map_orders.forEach((element, i) => {

        if (element.status == 1 || element.status == 2 || element !== undefined) {

            if (element.status == 1) {
                ind_orders.set(i, element);
            } else {
                //aca, siempre sera status = 2
                if (element.in_cart == element.quantity && element.status == 2) {
                    //obligatorio para la tabla "cart" y no se registra nada en la tabla de "Orders"
                    //Porque la cantidad de productos "in_cart" es la misma que el total de productos
                    
                    cart.set(ii, element);

                } else {
                    /*  Si entra aca significa que una cantidad de productos de esa orden, 
                        esta en la tabla de "cart" y la otra en la de "Orders" 
                    */
                    if (element.in_cart != 0) {

                        
                        let cart_element = {
                            id: element.id,
                            order_id: element.order_id,
                            price: parseFloat(element.price),
                            quantity: parseInt(element.in_cart),
                            order_variants: element.order_variants,
                            paid: element.paid
                        };

                        if (cart_element.quantity < 0) {
                            cart_element.quantity = 0;
                        }


                        cart.set(ii, cart_element);


                    }


                    element.quantity = parseInt(element.quantity) - parseInt(element.in_cart);

                    ind_orders.set(i, element);


                }

            }

        }
    });
    if(ind_orders && cart){
        return [ind_orders, cart];
    }else{
        return false;
    }
} //YAAA


function updateTableCart(cart) {


    second_table.innerHTML = "";

    cart.forEach((element, i) => {
        let cantidad = parseInt(element.quantity);
        let pagado = parseInt(element.paid);


        if (cantidad > 0 && pagado != cantidad) {
            second_table.innerHTML += ` <tr>
                                            <td style="max-width: 500px;">${element.order_variants}</td>
                                            <td>${element.price}</td>
                                            <td>${cantidad - pagado}</td>
                                            <td>${(element.price * element.quantity)}</td>
                                            <td><button class="btn btn-danger btn_cancel_cart" data-id="${i}">Cancel</button> </td>
                                        </tr>`;
        }
    });
    assign_FunctionCancelAr(cart);
}

function updateTableOrders(orders, cart) {


        first_table.innerHTML = "";

        orders.forEach((element, i) => {
            
            if (element.quantity > 0) {
                first_table.innerHTML += `<tr>
                                           <td style="max-width: 500px;">${element.order_variants}</td>
                                           <td class="price_element">${element.price}</td>
                                           <td class="quantity_element">${element.quantity}</td>
                                           <td class="total_element">${(element.price * element.quantity)}</td>
                                           <td class="buttons">
                                               <button data-id="${i}" class="btn btn-success">Accept</button>
                                               <button data-id="${i}" class="btn btn-danger btn_reject">Reject</button>
                                           </td>
                                           <td>
                                               <div class="buttons">
                                                   <button data-id="${i}" class="btn add_to_cart">Add to cart</button>
                                                   <button data-id="${i}" class="btn btn_delete">Delete</button>
                                               </div>
                                           </td>
                                       </tr>`;
            }
        });
    

    assign_functionDeleteOrder(orders);
    assign_FunctionAddCart(orders, cart);
    assign_FunctionRejectOrder(orders);

} //YAAAA

function assign_FunctionCancelAr(map_segunda_tabla){
    let btns_cancel_cart = document.querySelectorAll('.btn_cancel_cart');

    btns_cancel_cart.forEach(btn=>{
        let i = parseInt(btn.getAttribute('data-id'));
        btn.onclick = ()=>{cancel_ar(i)}
    });
}

function add_to_cart(key, orders, cart) {

    let element = orders.get(key);

    //console.log(element);
    

    Swal.fire({
        title: 'How many products do you want to add to the cart?',
        input: 'number',
        inputAttributes: {
            autocapitalize: 'off',
            required: true,
            min: 1,
            max: element.quantity
        },
        showCancelButton: true,
        confirmButtonText: 'Confirm!',
        showLoaderOnConfirm: true,


    }).then(res => {



        if (res.isConfirmed) {


            let cart_element = {
                id: element.id,
                order_id: element.order_id,
                price: parseFloat(element.price),
                quantity: parseInt(res.value),
                paid: 0,
                order_variants: element.order_variants
            };

            element.quantity = (parseInt(element.quantity) - parseInt(res.value));

            ////console.log(cart_element);

            if (element.quantity === 0) {
                orders.delete(key);

            }

            add_map_element(cart, cart_element);


            Swal.showLoading();
            DB_add_to_cart(element.id, res.value);
        }

    });
} //YAAA

function DB_add_to_cart(order_id, in_cart) {
    
    $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/shoppingcart/DB_add_to_cart',
        method: "POST",
        dataType: 'text',
        data: {
            '_token': $("meta[name='csrf-token']").attr("content"),
            'order_id': order_id,
            'in_cart': in_cart
        }
        
    }).done(resp => {
        cambio = true;
        Swal.fire({
            icon: "success",
            text: "Added to cart"
        });
        
    });
}// YAAAA

function DB_delete_order(map_primera_tabla, i) {

    let order_id = map_primera_tabla.get(i).id;

    Swal.fire({
        title: "Confirmation",
        text: "Do you really want to delete this order?",
        icon: "question"

    }).then(res => {
        if (res.isConfirmed) {


            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: '/shoppingcart/DB_delete_order',
                method: "POST",
                dataType: 'text',
                data: {
                    '_token': $("meta[name='csrf-token']").attr("content"),
                    'order_id': order_id
                }
            }).done(resp => {
                Swal.fire({
                    title: "Successfully deleted",
                    icon: "success"
                });

                if (map_primera_tabla.delete(i)) {
                    cambio = true;
                    //console.log(cambio);

                }


            });
        }
    })

} // YAAAA

function paidCart() {

    $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/shoppingcart/paidCart',
        method: "POST",
        dataType: 'text',
        data: {
            '_token': $("meta[name='csrf-token']").attr("content"),
            'cart': JSON.stringify(cart)
        }
    }).done(resp => {

        ////console.log(resp);
        if (resp == "1") {

            cart = [];
            updateTableCart(cart);
            updateTableOrders(orders, cart);

        }




    });

}

function reject_order(map_primera_tabla, i) {
    let order_id = map_primera_tabla.get(i).id;

    Swal.fire({
        title: "Confirmation",
        text: "Do you really want to reject this order?",
        icon: "question"

    }).then(res => {
        if (res.isConfirmed) {


            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: '/shoppingcart/reject_order',
                method: "POST",
                dataType: 'text',
                data: {
                    '_token': $("meta[name='csrf-token']").attr("content"),
                    'order_id': order_id
                }
            }).done(resp => {
                Swal.fire({
                    title: "Successfully rejected",
                    icon: "success"
                });

                if (map_primera_tabla.delete(i)) {
                    cambio = true;
                }else{
                    //console.log("Error");
                }




            });
        }
    })

} //YAAAA

function cancel_ar(i) {
    //console.log(cart.get(i));
    let html_buttons = `<button class="swal2-confirm swal2-styled" onclick="cancel_all(${i})">Yes</button>
                        <button class="swal2-confirm swal2-styled" onclick="cancel_partially(${i})">Cancel partially</button>`;
    Swal.fire({
        title: "Do you want to cancel this order from the cart?",
        icon: "question",
        showConfirmButton: false,
        footer: html_buttons
    });
}

function cancel_all(i) {
    Swal.showLoading();
    const cart_element = cart.get(i);

    let delete_element = new Promise((resolve, reject) => {
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/shoppingcart/DB_cancel_cart',
            method: "POST",
            dataType: 'text',
            data: {
                '_token': $("meta[name='csrf-token']").attr("content"),
                'id': cart_element.id
            }
        }).done(r => {

            //console.log(r);
            if (cart.delete(i)) {
                add_map_element(ind_orders, cart_element);
                resolve(true);

            } else {
                reject("error");
            }

        }).catch(e => {
            let msj = "Please try again later or reload the page. If the problem persists, contact support.";
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                showConfirmButton: false,
                text: 'Something went wrong!',
                footer: `<center>${msj}</center>`
            })
        });


    });

    delete_element.then(isOk => {
        if (isOk) {
            cambio = true;
            Swal.close();
        }
    }, error => {
        //console.log(error);
        alert("Error, the page will reload");
        document.location.reload();
    });
}
function cancel_partially(i) {
    let element = cart.get(i);
    Swal.fire({
        title: "How many do you want to cancel?",
        icon: "question",
        input: "number",
        inputAttributes: {
            autocapitalize: 'off',
            required: true,
            min: 1,
            max: element.quantity
        }
    }).then(res=>{
        Swal.showLoading();
        const quantity = parseInt(res.value);
        if(element.quantity == quantity){
            cancel_all(i);
        }else{
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: '/shoppingcart/DB_cancel_cart',
                method: "POST",
                dataType: 'text',
                data: {
                    '_token': $("meta[name='csrf-token']").attr("content"),
                    'id': element.id,
                    'quantity': quantity
                }
            }).done(r=>{
                var copia = JSON.parse(JSON.stringify(cart.get(i)));
                copia.quantity = quantity;
                add_map_element(ind_orders, copia);

                element.quantity = element.quantity - quantity;
                if(cambio = true){
                    Swal.fire({
                        icon: "success",
                        title: "Successfully canceled"
                    });
                }
            }); 
        }

    });
}

function add_map_element(map, element){
    var nuev = true;
    map.forEach(item=>{
        if(item.id == element.id){
            item.quantity = (element.quantity) + (item.quantity);
            nuev = false;
        }
    });

    if(nuev){
        let i = parseInt(map.size);
        map.set(i, element);
    } 
} //YAAAA