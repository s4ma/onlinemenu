function productsVariants(map_variants) {
    if (map_variants) {
        /* console.log(map_variants); */
        let variants_list = document.querySelector('.variants_list');

        let i = 0;

        let variant_name = map_variants.get('variant_name');
        variant_name.forEach((element, index) => {

            let div = document.createElement('div');
            variants_list.appendChild(div);
            div.className = "a_variant";
            div.innerHTML = `<h5>${element.slice(0, -2)}</h5>`;

            let end = i + parseInt(element.slice(-1));
            /* console.log("start " + i + " end " + end); */

            if (map_variants.get('dropdown')[index] == 1) {
                div.innerHTML += `<select onchange="report_c(this)" class="variant_options" name="select_a_option_${index}"></div>`;
            } else {
                div.innerHTML += `<div class="variant_options"></div>`;
            }

            let options = "";
            for (i; i < end; i++) {
                if (map_variants.get('dropdown')[index] == 1) {

                    options += `<option value="${map_variants.get('variant_price')[i]}" >${map_variants.get('variant_title')[i]} + ${currency} ${map_variants.get('variant_price')[i]}</option>`;

                } else {
                    options += `<label><input onclick="report_c(this)" type="checkbox" value="${map_variants.get('variant_price')[i]}" />${map_variants.get('variant_title')[i]} + ${currency} ${map_variants.get('variant_price')[i]}</label>`;

                }

            }
            div.querySelector('.variant_options').innerHTML = options;

            i = end;

        });
    }
}




if (typeof variants_map != "undefined") {
    
    productsVariants(variants_map);

    var ch = 1;
    setInterval(() => {
        totalprice.innerHTML = product_info.total_price;

        if (ch != product_info.has_change) {

            product_info.order_description = product_info.name;

            let select_variant_options = document.querySelectorAll('select.variant_options');
            let cbx_variant_options = document.querySelectorAll('div.variant_options input');

            let toAdd = 0;
            select_variant_options.forEach(select => {
                toAdd = toAdd + parseFloat(select.value);

                product_info.order_description += ', ' + select.previousElementSibling.innerHTML + ": " + select.options[select.selectedIndex].innerText;
            });

            product_info.total_price = parseFloat(product_info.original_price) + toAdd;

            cbx_variant_options.forEach(input => {
                if (input.checked == true) {
                    product_info.total_price = parseFloat(product_info.total_price) + parseFloat(input.value);
                    product_info.order_description += ', ' + input.parentNode.parentNode.previousSibling.innerText + ': ' + input.parentNode.innerText;
                }
            });

            ch = product_info.has_change;
            console.log(product_info.order_description);

        }

        if (product_info.total_price < product_info.original_price) {
            let variant_options = document.querySelectorAll('.variant_options input');
            product_info.total_price = product_info.original_price;

            variant_options.forEach(element => {
                element.checked = false;
            });
        }
    }, 500);


}


function report_c() {
    ch = 1;
}

var add_cart;
var cart = [];

function AddToCart() {
    if(typeof product_info == "undefined"){
        
    }
    add_cart = {
        id: product_info.id,
        order: product_info.order_description,
        quantity: 1,
        image_url: product_info.image_url,
        name: product_info.name,
        price: product_info.total_price,
        store_id: product_info.store_id
    };

    if (localStorage.getItem('cart') === null) {

        cart.push(add_cart);

        localStorage.setItem('cart', JSON.stringify(cart));

    } else {
        cart = JSON.parse(localStorage.getItem('cart'));

        cart.push(add_cart);

        localStorage.setItem('cart', JSON.stringify(cart));
    }


}

function goTo(url){

    document.location.href = url;

}