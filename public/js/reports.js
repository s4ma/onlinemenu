
$("document").ready(() => {

    var btn_today = document.getElementById("btn_today");
    var btn_other = document.getElementById("btn_other");

    btn_today.onclick = () => { todaysReport() }
    btn_other.onclick = () => { otherReports() }


});
function todaysReport() {
    Swal.showLoading();
    $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/admin/store/daily_report/',
        method: "POST",
        dataType: 'text',
        data: {
            '_token': $("meta[name='csrf-token']").attr("content"),
            'store_id': store_id
        }
    }).done(resp => {

        printRes(resp, "Today's report");


    });

}

function otherReports() {
    Swal.fire({
        title: 'Select a date',
        html: `<input required id="fil_date" type="date" max="${current_date}" min="2019-01-01">`
    }).then(res => {
        Swal.showLoading();
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/store/daily_report/',
            method: "POST",
            dataType: 'text',
            data: {
                '_token': $("meta[name='csrf-token']").attr("content"),
                'store_id': store_id,
                'fil_date': $('#fil_date').val()
            }
        }).done(resp => {
            if($('#fil_date').val()){
                let report = $('#fil_date').val() + " " + "report";
                printRes(resp, report );
            }else{

            }
            
        });
    });

}

function printRes(resp, tit) {
    let totalprice = 0;
    let totalquantity = 0;
    let days = JSON.parse(resp);
    console.log(days);
    let table = document.createElement('table');
    table.className = "table align-items-center table-flush";
    table.innerHTML = ` <tr>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Price</th>
                        </tr>`;


    days.forEach(element => {
        table.innerHTML += `<tr>
                                <td>${element.name} </td>
                                <td>${element.quantity} </td>
                                <td>${currency} ${element.price}</td>
                            </tr>`;
        totalprice = totalprice + parseFloat(element.price);
        totalquantity = totalquantity + element.quantity;
    });
    table.innerHTML += `<tr>
                            <td><b>Total</b></td>
                            <td>${totalquantity}</td>
                            <td>${currency} ${totalprice}</td>
                        </tr>`;

    Swal.fire({
        title: tit,
        html: table.outerHTML,
        confirmButtonColor: '#5e72e4',
        confirmButtonText: "OK!"
    });
}