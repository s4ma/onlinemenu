class Product {

    constructor(product_data, c) {
        this.product_data = product_data;
        this.c = c;
    }
    print(box) {

        let div = document.createElement('div');
        box.appendChild(div);
        div.outerHTML = `<div class="d-flex  align-items-center p-3 a_product" id="prod_${this.c}" data-id="${this.product_data.id}">
                            <a>
                                <div style="background-image: url(${this.product_data.image_url})" class="img-fluid"></div>
                            </a>
                            <a class="ml-3 text-dark text-decoration-none w-100">
                                <input type="text" class="order" disabled value="${this.product_data.order}" />
                                <div class="d-flex align-items-center">
                                    <p class="total_price font-weight-bold m-0">${currency} 
                                        <span id="price_${this.product_data.id}">${this.product_data.price}</span>
                                    </p>
                                    <div class="col-auto text-right">
                                        <button class="btn btn-danger btn-sm" onclick="remove(${this.c})" type="button"> Remove</button>
                                    </div>
                                    <div class="input-group input-spinner ml-auto cart-items-number">
                                        <div class="input-group-prepend" onclick="menos_uno(this, ${this.c})">
                                            <button class="btn btn-success btn-sm" type="button"> -</button>
                                        </div>

                                        <input type="text" class="cant" placeholder="" value="${this.product_data.quantity}">

                                        <div class="input-group-append" onclick="mas_uno(this, ${this.c})">
                                            <button class="btn btn-success btn-sm" type="button"> +</button>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>`;

    }
}

var products = JSON.parse(localStorage.getItem('cart'));
var PRODUCTS_LIST = document.querySelector('.PRODUCTS_LIST');


for (let c in products) {
    if (products[c] != null) {

        let producto = new Product(products[c], c);

        producto.print(PRODUCTS_LIST);

    }
}


function remove(id) {
    let toRemove = document.getElementById('prod_' + id);
    toRemove.outerHTML = "";

    delete products[id];

    updateCart();
}

var f_price = 0;

var update_price_cart = setInterval(() => {

    for (let i in products) {
        if (products[i] != null) {
            f_price = f_price + (products[i].price) * (products[i].quantity);
            totalprice.innerText = f_price;

        }
        if (localStorage.getItem('store_id') != null) {

            let store_id = localStorage.getItem('store_id');
            let cart = JSON.parse(localStorage.getItem('cart'));

            for (let i in cart) {
                if (typeof cart[i] == "object" && cart[i] != null) {
                    if (parseInt(cart[i].store_id) != parseInt(store_id)) {
                        delete cart[i];
                        localStorage.setItem('cart', JSON.stringify(cart));
                    }
                }
            }

        }
    }
    f_price = 0;

}, 1000);

function menos_uno(this_button, c) {

    this_button.nextElementSibling.value--;

    if (this_button.nextElementSibling.value < 0) {
        this_button.nextElementSibling.value = 0;
    }

    products[c].quantity = parseInt(this_button.nextElementSibling.value);
    updateCart();

}
function mas_uno(this_button, c) {
    this_button.previousElementSibling.value++;
    products[c].quantity = parseInt(this_button.previousElementSibling.value);
    updateCart();
}


function updateCart() {
    localStorage.setItem('cart', JSON.stringify(products));
}



function makeOrder() {
    Swal.fire({
        title: "¿Confirm order?",
        text: `${text_order.innerText}`,
        icon: "question"
    }).then(res => {
        if (res.isConfirmed) {
            localStorage.removeItem('cart');
            PRODUCTS_LIST.innerHTML = "";
            
            Swal.fire(
                'Order placed',
                'Your order has been placed successfully',
                'success'
            );

            (async function () {
                let strOrder = "";
                let store = document.querySelector('#root');
                let order_resume = [];

                for (let i in products) {
                    if (products[i] != null) {
                        if (products[i].quantity != 0) {
                            order_resume.push(products[i]);

                            strOrder = strOrder + "\n - " + products[i].order;
                        }

                    }
                }

                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url: '/shoppingcart/makeorder',
                    method: "POST",
                    dataType: 'text',
                    data: {
                        '_token': $("meta[name='csrf-token']").attr("content"),
                        'order': strOrder,
                        'id_store': store.getAttribute('data-id'),
                        'store_name': store.getAttribute('data-name'),
                        'comment': txtComment.value,
                        'table_name': store.getAttribute('data-table'),
                        'order_details': JSON.stringify(order_resume),
                        'subtotal': parseFloat(totalprice.innerText)
                    }
                }).done(resp => {
                    console.log(parseFloat(totalprice.innerText));
                    totalprice.innerText = 0;
                });

            }())

        }
    });
}