<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QR Code {{$table->table_name}} </title>
</head>
<style>
    body {
        display: flex;
        justify-content: center;
        background-color: #f0f2f5;
    }

    .page_content {
        margin-top: 50px;
        background-color: #f7f7f7;
        border-radius: 4px;
        box-shadow: 0 1rem 3rem rgba(0, 0, 0, .175);
        min-width: 300px;
        max-width: 360px;
    }

    .part_1 {
        width: 100%;
        text-align: center;
        border-bottom: 1px solid #ccc;
        padding-bottom: 8px;
    }

    h1 {
        font-family: 'Ubuntu', sans-serif;
        font-size: 2.5rem;
    }

    .part_2 {
        padding: 20px;
        display: flex;
        flex-direction: column;
        align-items: center;
        text-align: center;
        background-color: #fff;
    }

    strong {
        font-family: sans-serif;
        font-size: 13px;
    }

    strong.l {
        color: #6c757d;
    }
</style>
<body>
    <div class="page_content">
        <div class="part_1">
            <div class="store_name">
                <h1> {{$store->store_name}} </h1>
            </div>
            <div class="qr_code">
                {!! QrCode::size(250)->generate($url_qr); !!}
            </div>

        </div>
        <div class="part_2">
            <div class="info">
                <strong>Get our MENU on your PHONE</strong>
            </div>
            <h1>
                <span>Table: </span>
                <span style="color:red"> {{$table->table_name}} </span>
            </h1>
            <strong class="l">Simply open your phone's camera and point it at the code.</strong>
            <strong class="l">Tap on the link that appears.</strong>
        </div>
    </div>

</body>
</html>
