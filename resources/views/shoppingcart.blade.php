<?php
    session_start();

    $store_name = $_SESSION['store_name'];
    $table_name = $_SESSION['table_name'];

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <meta name="csrf-token" content="{{csrf_token()}}" />
    <title>Cart</title>
    <script>
        localStorage.setItem('store_id', {{$id_store}});

        const currency = '{{$currency}}';
        
        if (localStorage.getItem('store_id') != null) {

            let store_id = localStorage.getItem('store_id');
            let cart = JSON.parse(localStorage.getItem('cart'));

            for (let i in cart) {
                if (typeof cart[i] == "object" && cart[i] != null) {
                    if (parseInt(cart[i].store_id) != parseInt(store_id)) {
                        delete cart[i];
                        localStorage.setItem('cart', JSON.stringify(cart));
                    }
                }
            }

        }

    </script>
    <style>
        .total {
            margin-bottom: 50px;
        }

        footer {
            background: #fff;
            position: fixed;
            width: 100%;
            bottom: 0;
            display: grid;
            box-shadow: -2px -2px 10px rgba(0, 0, 0, .075);
            grid-template-columns: 1fr 1fr 1fr;
            padding: 2px 0px 7px;
        }

        .item_footer_img {
            width: 30px;
            height: 30px;
            background-size: 18px;
            background-repeat: no-repeat;
            background-position: center;
        }

        .item_footer {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .item_footer strong {
            font-family: sans-serif;
            font-size: 13px;
            color: #4a4f55;
        }

        .img-fluid {
            width: 90px;
            height: 90px;
            background-position: center;
            background-size: cover;
        }

        input.order {
            border: none;
            background: none;
            font-size: 1.3em;
            font-family: 'Ubuntu', sans-serif;
            position: relative;
            right: 10px;
            bottom: 10px;
            width: 200px;
        }

        input.cant {
            width: 30px;
            text-align: center;
        }

    </style>
</head>
<body>
    <div id="root" data-id="{{$id_store}}" data-name="{{$store_name}}" data-table="{{$table_name}}">
        <div>
            <div class="fixed-bottom-padding">
                <div class="p-3 border-bottom">
                    <div class="d-flex align-items-center">
                        <h5 class="font-weight-bold m-0">Cart</h5>
                    </div>
                </div>
                <div class="osahan-body">
                    <div class="PRODUCTS_LIST">

                    </div>
                    <div class="p-3">


                        <div class="form-group">
                            <label for="exampleInputNEWPassword1">Comment </label>
                            <textarea id="txtComment" placeholder="Write Your Comment for Store Owners" class="form-control" name="comments"></textarea>
                        </div>


                    </div>
                </div>
                <div class="p-3 mt-5 total">
                    <a class="text-decoration-none" onclick="makeOrder()">
                        <div class="rounded shadow bg-success d-flex align-items-center p-3 text-white">
                            <div class="more">
                                <h6 class="m-0" id="text_order">Total: {{$currency}} <span id="totalprice"></span></h6>
                                <p class="small m-0">Confirm your order.</p>
                            </div>
                            <div class="ml-auto">
                                <i class="icofont-simple-right"></i>
                            </div>
                        </div>
                    </a></div>
            </div>







            {{-- FOOTER SECTION --}}
            <footer>
                <div class="item_footer" onclick="goTo(`/table/{{$id_store}}/{{$id_table}}`)">
                    <div class="item_footer_img" style="background-image: url({{asset('svg/icon-buy-gray.svg')}})"></div>
                    <strong>Shop</strong>
                </div>
                <div class="item_footer">
                    <div class="item_footer_img" style="background-image: url({{asset('svg/icon-cart-green.svg')}})"></div>
                    <strong>Cart</strong>
                </div>
                <div class="item_footer" onclick="goTo(`/table/{{$id_store}}/{{$id_table}}/orders`)">
                    <div class="item_footer_img" style="background-image: url({{asset('svg/icon-order-gray.svg')}})"></div>
                    <strong>My Order</strong>
                </div>
            </footer>
        </div>
    </div>
    </div>
    <script>
        function goTo(url) {
            document.location.href = url;
        }

    </script>
</body>
</html>

<script src="{{asset('js/cart.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
