<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Orders</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <meta name="csrf-token" content="{{csrf_token()}}" />

    <style>
        footer {
            background: #fff;
            position: fixed;
            width: 100%;
            bottom: 0;
            display: grid;
            box-shadow: -2px -2px 10px rgba(0, 0, 0, .075);
            grid-template-columns: 1fr 1fr 1fr;
            padding: 2px 0px 7px;
        }

        .item_footer_img {
            width: 30px;
            height: 30px;
            background-size: 18px;
            background-repeat: no-repeat;
            background-position: center;
        }

        .item_footer {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .item_footer strong {
            font-family: sans-serif;
            font-size: 13px;
            color: #4a4f55;
        }

    </style>
</head>
<body>

</body>
</html>

<div id="root">
    <div>
        <div class="fixed-bottom-padding">

            <div class="p-3 border-bottom">
                <div class="d-flex align-items-center">
                    <h5 class="font-weight-bold m-0">My Order</h5>
                </div>
            </div>
            <main>
                <div class="order-body px-3 pt-3 ORDERS_CONTAINER">
                    <h6 class="mb-2">Current Order</h6>

                    @foreach($orders as $key)

                    <?php
                                    switch($key->status){
                                        case 1 : $status = "PENDING";break;
                                        case 2 : $status = "PROCESSING";break;
                                        case 3 : $status = "REJECTED";break;
                                        default: $status = "Default"; break;
                                    }
                                ?>
                    <div class="pb-3 A_ORDER" onclick="order(`{{$key->details}}`)">
                        <a class="text-decoration-none text-dark  ">
                            <div class="p-3 rounded shadow-sm bg-white ">
                                <div class="d-flex align-items-center mb-3">
                                    <p class="text-white py-1 px-2 mb-0 rounded small bg-warning">
                                        {{$status}}
                                    </p>
                                    <p class="text-muted ml-auto small mb-0">
                                        <i class="icofont-clock-time">
                                        </i> {{$key->created_at}}
                                        <b class="text-danger">

                                        </b>
                                    </p>
                                </div>
                                <div class="d-flex">
                                    <p class="text-muted m-0">
                                        Order ID
                                        <br>
                                        <span class="text-dark font-weight-bold">
                                            {{$key->order_unique_id}}
                                        </span>
                                    </p>
                                    <p class="text-muted m-0 ml-auto">
                                        Table
                                        <br>
                                        <span class="text-dark font-weight-bold">{{$table_name}}</span>
                                    </p>
                                    <p class="text-muted m-0 ml-auto">
                                        Total price
                                        <br>
                                        <span class="text-dark font-weight-bold">{{$key->total}}</span>
                                    </p>
                                </div>
                            </div>
                        </a></div>
                    @endforeach


                </div>
                <div class="order-body px-3 pt-3">
                    <h6 class="mb-2">Completed Order</h6>
                </div>
                {{-- FOOTER SECTION --}}
                <footer>
                    <div class="item_footer" onclick="goTo(`/table/{{$id_store}}/{{$id_table}}`)">
                        <div class="item_footer_img" style="background-image: url({{asset('svg/icon-buy-gray.svg')}})"></div>
                        <strong>Shop</strong>
                    </div>
                    <div class="item_footer" onclick="goTo(`/shoppingcart/{{$id_store}}/{{$id_table}}`);">
                        <div class="item_footer_img" style="background-image: url({{asset('svg/icon-cart-gray.svg')}})"></div>
                        <strong>Cart</strong>
                    </div>
                    <div class="item_footer" onclick="goTo(`/table/{{$id_store}}/{{$id_table}}/orders`)">
                        <div class="item_footer_img" style="background-image: url({{asset('svg/icon-order-green.svg')}})"></div>
                        <strong>My Order</strong>
                    </div>
                </footer>
            </main>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    function goTo(url) {
        document.location.href = url;
    }

    function order(details) {
        Swal.fire({
            title: "Order details"
            , text: `${details}`
            , icon: "info"
        }).then(res => {
            if (res.isConfirmed) {

            }

        });
    }

</script>
