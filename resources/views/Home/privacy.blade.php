@extends('Home.home_layout.registerpage')



@section('register_content')


   <style>

       h1{
           color: #ffffff;
       }

       h2{
           color: #ffffff;
       }
       h3{
           color: #ffffff;
       }
      h4 {
           color: #ffffff;
       }
       h5{
           color: #ffffff;
       }
       h6{
           color: #ffffff;
       }
   </style>

    <nav id="navbar-main" class="navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="{{route('home')}}">
                <img src="{{asset($account_info !=NULL ? $account_info->application_logo:'assets_home/images/logo/logo.png')}}">
            </a>
            <a href="{{route('store_register')}}" class="btn btn-neutral btn-icon">

                <span class="nav-link-inner--text">Register</span>
            </a>

        </div>
    </nav>
    <!-- Main content -->


    <div class="main-content">
        <!-- Header -->
        <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
            <div class="container">
                <div class="header-body text-center mb-7">
                    <div class="row justify-content-center">
                        <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                            <h1 class="text-white">Privacy Policy</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="separator separator-bottom separator-skew zindex-100">
                <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">

                @foreach($home as $data)


                    <div class="d-flex justify-content-lg-center px-3 mt-5">
                        <div>
                            <div class="icon icon-shape bg-gradient-white shadow rounded-circle text-primary">
                                <i class="ni ni-building text-primary"></i>
                            </div>
                        </div>
                        <div class="col-lg-6" style="color: #ffffff;">
                            <p class="text-white" style="color: #ffffff;">
                                {!! $data->privacy_policy !!}

                        </div>
                    </div>

                @endforeach



            </div>

        </div>









@endsection
