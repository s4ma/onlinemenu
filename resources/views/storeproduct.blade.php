<?php
    $protocol   = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
    $host       = $_SERVER["HTTP_HOST"];
    $page_dir   = $protocol . $host . "/";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" >
    <title>{{$the_product->name}}</title>
    <?php
        if(isset($variants->id)){
            print "<script>";

            print  "var variants_map =  new Map(); \n
                    variants_map.set('id', " . $variants->id . ");
                    variants_map.set('id_product',      " . $variants->id_product    . ");
                    variants_map.set('variant_name',    " . $variants->variant_name  . ");
                    variants_map.set('variant_title',   " . $variants->variant_title . ");
                    variants_map.set('variant_price',   " . $variants->variant_price . ");
                    variants_map.set('dropdown',        " . $variants->dropdown      . ");  
                    variants_map.set('visibility',      " . $variants->visibility    . ");
                    ";

            print "</script>";
        }else{
                print "<script>";
                    print "var variants_map = false; ";
                print "</script>";
        }
        
    ?>
    <script>
        const currency = '{{$currency}}';
        var product_info = {};

        product_info.total_price = {{$the_product->price}} ;
        product_info.original_price = {{$the_product->price}} ;
        product_info.name = "{{$the_product->name}}";
        product_info.has_change = 0;
        product_info.order_description = "{{$the_product->name}}";
        product_info.id = {{$the_product->id}};
        product_info.image_url = "{{$page_dir}}{{$the_product->image_url}}";
        product_info.store_id = {{$the_product->store_id}};

        localStorage.setItem('store_id', {{$the_product->store_id}} );

    </script>
</head>
<body>
    <style>
        .a_variant {
            margin: 1em;
        }

        .variant_options label {
            display: block;
        }

        select.variant_options {
            padding: 5px;
            border: 2px solid #28a745;
            background: #fff;
        }

        div.variant_options label {
            margin: 5px;
        }

        .variant_options input {
            margin: 0 8px;
            transform: scale(1.5);
        }

        .variantes h4 {
            margin-left: 10px;
        }

        .variantes {
            margin-bottom: 24px;
        }

        .btn_cart {
            background-image: url(/svg/icon-cart-gray.svg);
            background-position: center;
            background-size: 26px;
            background-repeat: no-repeat;
            padding: 30px 54px !important;
        }

        .btn_add_to_cart {
            border-radius: 0px;
            padding: 17px 0;
        }

        .mb-6 {
            margin-bottom: 50px;
        }

        .bg-image {
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;
        }

    </style>


    <div id="root">
        <div>
            <div class="fixed-bottom-padding">
                <div class="p-3 bg-white">
                    <div class="d-flex align-items-center">
                        <a class="font-weight-bold text-success text-decoration-none">
                            <i class="icofont-rounded-left back-page"></i> Back</a></div>
                    <div class="notifications-wrapper"></div>
                    <div class="osahan-body">
                        <div class="p-3 osahan-categories"></div>
                    </div>
                </div>
                <div class="px-3 bg-white pb-3">
                    <div class="pt-0">
                        <h2 class="font-weight-bold">{{$the_product->name}}</h2>
                        <p class="font-weight-light text-dark m-0 d-flex align-items-center">Product MRP :
                            <b class="h6 text-dark m-0">{{$currency}} <strong id="totalprice">{{$the_product->price}}</strong></b>
                            <span class="badge badge-danger ml-2">AVAILABLE</span>
                            <span class="badge badge-success ml-2">RECOMMENDED</span></p><a href="review.html">
                            <div class="rating-wrap d-flex align-items-center mt-2"></div>
                        </a>
                    </div>
                    <div class="pt-2">
                        <div class="row"></div>
                    </div>
                </div>
                <div class="osahan-product">
                    <div class="product-details">
                        <div class="recommend-slider py-1 slick-initialized slick-slider">
                            <div class="slick-list draggable bg-image" style="padding: 0px 50px; height: 220px; background-image: url({{$page_dir}}{{$the_product->image_url}});">
                                <div class="slick-track" style="opacity: 1; width: 220px; transform: translate3d(0px, 0px, 0px);">
                                    <div class="osahan-slider-item m-2 slick-slide slick-current slick-active slick-center" data-slick-index="0" aria-hidden="false" style="width: 204px;" tabindex="0">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details">
                            <div class="p-3">
                                <p class="font-weight-bold mb-2">Product Details</p>
                                <p class="text-muted small">{{$the_product->description}}</p>
                            </div>
                        </div>
                        {{-- CONTINUAR ACA --}}

                        <div class="variantes">
                            <h4>Product variants</h4>
                            <div class="variants_list">
                                <!-- List of variants by js -->
                            </div>
                        </div>

                        <script src="{{asset('js/product.js')}}"></script>

                        <div class="fixed-bottom pd-f bg-white d-flex align-items-center border-top ">
                            <a href="{{$page_dir}}shoppingcart/{{$id_store}}/{{$id_table}}" class="btn-warning py-3 px-5 h4 m-0 btn_cart">
                                <i class="icofont-cart"></i>
                            </a>
                            <a class="btn btn-success btn-block btn_add_to_cart" onclick="AddToCart()">Add To Cart</a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="title d-flex align-items-center mb-3 mt-3 px-3">
                        <h6 class="m-0">Maybe You Like this.</h6>
                    </div>
                    <div class="pick_today px-3 mb-6">

                        <div class="col-6 search" id="category-undefined" style="margin-bottom: 15px;">
                            <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm">
                                <div class="list-card-image">
                                    <div class="member-plan position-absolute"><span class="badge m-2 badge-warning bg-success" style="color: rgb(255, 255, 255);">REC</span></div>
                                    <div class="p-3"><a class="text-dark"></a><a href="/store/1386d6b1dfb7fe6b8a79a3d63e5f8f3933dfa828/product/details/9"><img src="http://onlinemenu.kiubo.ch/storage/stores/category/images//rTwnbWOGxixzbBGRhj26MMy98P5lwvNbEUbFyWXY.jpeg" class="img-fluid item-img w-100 mb-3"></a>
                                        <h6>Pizza hawaiana</h6>

                                        <div class="d-flex align-items-center"><a href="/store/1386d6b1dfb7fe6b8a79a3d63e5f8f3933dfa828/product/details/9" class="text-dark">
                                                <h6 class="price m-0 text-success">{{$currency}} 10</h6>
                                            </a><a class="btn btn-success btn-sm ml-auto">+</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
