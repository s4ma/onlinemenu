
@extends("admin.adminlayout")

@section("admin_content")

    <div class="container-fluid mt--6">

        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="left-side-tabs">
                    <div class="dashboard-left-links">
                        <a href="{{route('settings')}}" class="user-item">Site Settings</a>
                        <a href="{{route('account_settings')}}" class="user-item ">Account Settings</a>
                        <a href="{{route('paymentsettings')}}" class="user-item"> Payment Settings</a>
                        <a href="#" class="user-item active"> Privacy Policy</a>

                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-6">
                <div class="card card-static-2 mb-30">
                    <div class="card-title-2">
                        <h4>Privacy Policy</h4>
                    </div>
                    <div class="card-body">
                        @if(session()->has("MSG"))
                            <div class="alert alert-{{session()->get("TYPE")}}">
                                <strong> <a>{{session()->get("MSG")}}</a></strong>
                            </div>
                        @endif
                        @if($errors->any()) @include('admin.admin_layout.form_error') @endif




                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="{{route('clear_app')}}" class="btn btn-primary btn-lg">Clear App</a>
                                    </div>
                                </div>


                            </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
