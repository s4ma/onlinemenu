@extends("restaurants.layouts.restaurantslayout")

@section("restaurantcontant")

<style>
    .add_variants {
        width: 100%;
        height: auto;
        background: #f3f4fa;
        display: none;
        padding: 10px 0px;
    }

    .a_variant {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        margin: 8px 0px;
    }


    .a_variant input {
        border: 1px solid #ccc;
        border-radius: 4px;
        margin: 4px 10px;
        color: #2f3a46;
    }

    .a_variant span {
        margin: 4px 10px;
    }

    #btnAddVariant {
        text-align: center;
        width: 150px;
        background: #fbb140;
        color: #fff;
    }

    .center_button_variant {
        display: flex;
        justify-content: center;
        margin: 10px 0;
    }

    .btnClose_ {
        background-image: url(http://127.0.0.1:8000/assets/images/delete.svg);
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
        color: transparent;
        padding: 1em;
    }

    .btn.btnClose_:hover {
        box-shadow: none;
    }

    .title_list {
        width: 100%;
        height: auto;
        justify-content: center;
        margin: 10px 0 10px 0;
        display: flex;
    }

    .title_list input {
        padding: 10px 3px;
    }

    select.select_mode_view {
        height: 43px;
        margin-top: 4px;
        border: none;
        background: #fff;
        border-radius: 5px;
        color: #525f7f;
    }
</style>
<?php
$url        = $_SERVER['REQUEST_URI'];
$id_product = intval(preg_replace("/[^0-9]+/", "", $url), 10);

$connection = new mysqli("localhost", "root", "", "shopinnstore");
$sql = "SELECT * FROM `products_variants` WHERE `products_variants`.`id_product` = $id_product";
$query = mysqli_query($connection, $sql);


if ($assoc = mysqli_fetch_assoc($query)) {
    print "<script>";

    print    "   var variants_map =  new Map(); \n
                    variants_map.set('id', " . $assoc['id'] . ");
                    variants_map.set('id_product',      " . $assoc['id_product']    . ");
                    variants_map.set('variant_name',    " . $assoc['variant_name']  . ");
                    variants_map.set('variant_title',   " . $assoc['variant_title'] . ");
                    variants_map.set('variant_price',   " . $assoc['variant_price'] . ");
                    variants_map.set('dropdown',        " . $assoc['dropdown']      . ");  
                    variants_map.set('visibility',     " . $assoc['visibility']    . ");
            ";

    print "</script>";
} else {
    print "<script>";
    print "var variants_map = false; ";
    print "</script>";
}


?>



<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
            <h3 class="mb-0">Edit Products</h3>
            @if(session()->has("MSG"))
            <div class="alert alert-{{session()->get("TYPE")}}">
                <strong> <a>{{session()->get("MSG")}}</a></strong>
            </div>
            @endif
            @if($errors->any()) @include('admin.admin_layout.form_error') @endif
        </div>
        <!-- Card body -->
        <div class="card-body">
            <form method="post" id="form_page" action="{{route('store_admin.edit_products',['id'=>$data->id])}}" enctype="multipart/form-data">
                {{csrf_field()}}
                @method('PATCH')
                <!-- Form groups used in grid -->
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols1Input">Image</label>


                            <div class="custom-file">
                                <input name="image_url" class="file-name input-flat ui-autocomplete-input" type="file" readonly="readonly" placeholder="Browses photo" autocomplete="off">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols2Input">Product Name</label>
                            <input type="text" value="{{$data->name}}" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols2Input">Price</label>
                            <input type="number" value="{{$data->price}}" name="price" class="form-control" required>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group" style="display: none;">
                            <label class="form-control-label" for="example3cols2Input">Cooking Time no</label>
                            <input type="number" value="{{$data->cooking_time}}" name="cooking_time" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label style="position: relative;top:40px;user-select: none;">
                                <input style="transform: scale(1.5);margin-right: 6px" type="checkbox" name="product_variant" id="cbxProductVariants">
                                Product variants
                            </label>
                        </div>
                    </div>
                    <div class="add_variants">
                        <div class="variants_list">
                            <!-- List of variants by js -->

                        </div>
                        <div class="center_button_variant">
                            <span class="btn" id="btnAddVariant" style="text-align: center;">Add variant</span>
                        </div>
                    </div>

                    <script>
                        var product_variant = {
                            add_new: (element_variants_list, value = "") => {
                                let div = document.createElement('div');
                                element_variants_list.appendChild(div);

                                div.outerHTML =
                                    `<div class="a_variant">
                                        <input type="text" value="${value}" pattern = "[a-zA-Z0-9 ]+" title="Letters from a to z" required name="variant_name[]" class="variant_name" placeholder="Variant name">
                                        <input type="text" pattern = "[a-zA-Z0-9 ]+" title="Letters from a to z" required name="variant_title[]" class="variant_title" placeholder="Title">
                                        <input type="number" required name="variant_price[]" class="variant_price" placeholder="Additional price">
                                        <select class="select_mode_view" name="dropdown[]">
                                            <option selected value="1">Dropdown</option>
                                            <option value="2">Checkboxes</option>
                                        </select>
                                        <span class="btn add_title" onclick="product_variant.add_title(this);" style="background-color: #fff;">Add title</span>
                                        <span class="btn btnClose_" onclick="product_variant.delete(this)"></span>
                                    </div>`;
                            },
                            add_title: this_element => {
                                let div = document.createElement('div');
                                this_element.parentNode.appendChild(div);
                                div.outerHTML =
                                    `<div class="title_list">
                                        <input type="text" pattern = "[a-zA-Z0-9 ]+" title="Letters from a to z" required name="variant_title[]" class="variant_title" placeholder="Title">
                                        <input type="number" required name="variant_price[]" class="variant_price" placeholder="Additional price">
                                        <span class="btn btnClose_" onclick="product_variant.delete(this)"></span>
                                     </div>
                                    `;
                            },
                            delete: this_element => {
                                this_element.parentNode.outerHTML = "";
                            },
                            active: element_add_variants => {
                                if (cbxProductVariants.checked == true) {
                                    element_add_variants.style.display = "block";
                                } else {
                                    element_add_variants.style.display = "none";
                                }
                            },
                            recover: (variants_list, add_variants, cbxProductVariants) => {
                                if (variants_map) {

                                    if(variants_map.get("visibility") == 1){
                                        cbxProductVariants.checked = true;    
                                    }else{
                                        cbxProductVariants.checked = false;    
                                    }
                                        
                                    product_variant.active(add_variants);

                                    var i = 0;

                                    variants_map.get("variant_name").forEach((element,index) => {

                                        product_variant.add_new(variants_list, element.slice(0,-2));
                                        
                                        let end = i + parseInt(element.slice(-1));
                                        console.log("start "+ i + " end " +end);

                                        for(i ; i < end-1 ; i++){
                                            product_variant.add_title(document.querySelectorAll(".add_title")[index]);
                                        }


/*                                         let a_variant = document.querySelectorAll(".a_variant")[index];
                                        let variant_title = a_variant.querySelectorAll(".variant_title");

                                        variant_title.forEach(element=>{
                                            element.value = variants_map.get("variant_title")[end];
                                        }); */
                                        
                                        i = end;
                                    });
                                        let variant_title = document.querySelectorAll(".variant_title");
                                        let variant_price = document.querySelectorAll(".variant_price");
                                        let select_mode_view = document.querySelectorAll(".select_mode_view");

                                        variant_title.forEach((element, c)=>{
                                            element.value = variants_map.get("variant_title")[c];
                                        });
                                        variant_price.forEach((element, c)=>{
                                            element.value = variants_map.get("variant_price")[c];
                                        });
                                        select_mode_view.forEach((element, c)=>{
                                            element.value = variants_map.get("dropdown")[c];
                                        });

                                }

                            }
                        }





                        var cbxProductVariants = document.querySelector("#cbxProductVariants");
                        var add_variants = document.querySelector(".add_variants");
                        var variants_list = document.querySelector(".variants_list");
                        var form_page = document.getElementById("form_page");

                        cbxProductVariants.addEventListener("click", () => {
                            product_variant.active(add_variants);
                        });

                        btnAddVariant.addEventListener('click', () => {
                            product_variant.add_new(variants_list);
                        });


                        form_page.onsubmit = () => {
                            let variant_name = document.querySelectorAll(".variant_name");

                            variant_name.forEach(element => {
                                let titles = element.parentNode.querySelectorAll('div').length;
                                titles = (titles + 1);

                                element.value += `:${titles}`;


                            });

                        }
                        
                        product_variant.recover(variants_list, add_variants, cbxProductVariants);
                    </script>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Select
                                Category</label>
                            <select class="form-control" name="category_id" required>

                                @foreach($category as $cat)
                                <option value="{{ $cat->id }}" {{$data->category_id ==$cat->id ? "selected":null }}>{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Is Enabled</label>
                            <select class="form-control" name="is_active" required>
                                <option value="1" {{$data->is_active == 1 ? "selected":NULL}}>Yes</option>
                                <option value="0" {{$data->is_active == 0 ? "selected":NULL}}>No</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Is Recommended</label>
                            <select class="form-control" name="is_recommended" required>
                                <option value="1" {{$data->is_recommended == 1 ? "selected":NULL}}>Yes</option>
                                <option value="0" {{$data->is_recommended == 0 ? "selected":NULL}}>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Is Veg</label>
                            <select class="form-control" name="is_veg" required>
                                <option value="1" {{$data->is_veg == 1 ? "selected":NULL}}>Yes</option>
                                <option value="0" {{$data->is_veg == 0 ? "selected":NULL}}>No</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Description</label>
                            <textarea class="form-control" name="description" rows="3" required>{{$data->description}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Update Product</button>
                    </div>


                </div>

            </form>
        </div>
    </div>





    {{-- <div class="row">--}}
    {{-- <div class="col-lg-12">--}}
    {{-- <div class="card alert">--}}
    {{-- <div class="card-header">--}}
    {{-- <h4>Edit Products</h4>--}}
    {{-- @if(session()->has("MSG"))--}}
    {{-- <div class="alert alert-{{session()->get("TYPE")}}">--}}
    {{-- <strong> <a>{{session()->get("MSG")}}</a></strong>--}}
    {{-- </div>--}}
    {{-- @endif--}}
    {{-- @if($errors->any()) @include('admin.admin_layout.form_error') @endif--}}
    {{-- </div>--}}
    {{-- <div class="card-body">--}}
    {{-- <div class="menu-upload-form">--}}
    {{-- <form class="form-horizontal" method="post" action="{{route('store_admin.edit_products',['id'=>$data->id])}}" enctype="multipart/form-data">--}}
    {{-- {{csrf_field()}}--}}
    {{-- @method('PATCH')--}}
    {{-- <div class="form-group">--}}
    {{-- <label class="col-sm-2 control-label">Photo</label>--}}
    {{-- <div class="col-sm-10">--}}
    {{-- <div class="form-control file-input dark-browse-input-box">--}}

    {{-- <input name="image_url"  class="file-name input-flat ui-autocomplete-input" type="file" readonly="readonly" placeholder="Browses photo" autocomplete="off">--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- <div class="form-group">--}}
    {{-- <label class="col-sm-2 control-label">Name</label>--}}
    {{-- <div class="col-sm-10">--}}
    {{-- <input type="text" value="{{$data->name}}" name="name" class="form-control" placeholder="Product Name">--}}
    {{-- </div>--}}
    {{-- </div>--}}

    {{-- <div class="form-group">--}}
    {{-- <label class="col-sm-2 control-label">Price</label>--}}
    {{-- <div class="col-sm-10">--}}
    {{-- <input type="number" name="price" value="{{$data->price}}" class="form-control" placeholder="Price">--}}
    {{-- </div>--}}
    {{-- </div>--}}

    {{-- <div class="form-group">--}}
    {{-- <label class="col-sm-2 control-label">Cooking Time</label>--}}
    {{-- <div class="col-sm-10">--}}
    {{-- <input type="text" name="cooking_time" value="{{$data->cooking_time}}" class="form-control" placeholder="Cooking Time">--}}
    {{-- </div>--}}
    {{-- </div>--}}

    {{-- <div class="form-group">--}}
    {{-- <label class="col-sm-2 control-label">Description</label>--}}
    {{-- <div class="col-sm-10">--}}
    {{-- <textarea class="form-control" name="description" rows="3" placeholder="Description">{{$data->description}}</textarea>--}}
    {{-- </div>--}}
    {{-- </div>--}}



    {{-- <div class="form-group">--}}
    {{-- <label class="col-sm-2 control-label">Select Category</label>--}}
    {{-- <div class="col-sm-10">--}}
    {{-- <select class="form-control" name="category_id">--}}

    {{-- <option value="{{$data->category_id}}">{{$data->category_id}}</option>--}}
    {{-- @foreach($category as $cat)--}}
    {{-- <option value="{{ $cat->id }}">{{ $cat->name }}</option>--}}



    {{-- @endforeach--}}

    {{-- </select>--}}
    {{-- </div>--}}
    {{-- </div>--}}


    {{-- <div class="form-group">--}}
    {{-- <label class="col-sm-2 control-label">Is Enabled</label>--}}
    {{-- <div class="col-sm-10">--}}
    {{-- <select class="form-control" name="is_active">--}}
    {{-- <option value="1" {{$data->is_active == 1 ? "selected":NULL}}>Yes</option>--}}
    {{-- <option value="0" {{$data->is_active == 0 ? "selected":NULL}}>No</option>--}}

    {{-- </select>--}}
    {{-- </div>--}}
    {{-- </div>--}}

    {{-- <div class="form-group">--}}
    {{-- <label class="col-sm-2 control-label">Is Recommended</label>--}}
    {{-- <div class="col-sm-10">--}}
    {{-- <select class="form-control" name="is_recommended">--}}
    {{-- <option value="1" {{$data->is_recommended == 1 ? "selected":NULL}}>Yes</option>--}}
    {{-- <option value="0" {{$data->is_recommended == 0 ? "selected":NULL}}>No</option>--}}

    {{-- </select>--}}
    {{-- </div>--}}
    {{-- </div>--}}


    {{-- <div class="form-group">--}}
    {{-- <label class="col-sm-2 control-label">Is Veg</label>--}}
    {{-- <div class="col-sm-10">--}}
    {{-- <select class="form-control" name="is_veg">--}}
    {{-- <option value="1" {{$data->is_veg == 1 ? "selected":NULL}}>Yes</option>--}}
    {{-- <option value="0" {{$data->is_veg == 0 ? "selected":NULL}}>No</option>--}}

    {{-- </select>--}}
    {{-- </div>--}}
    {{-- </div>--}}








    {{-- <div class="form-group">--}}
    {{-- <div class="col-sm-offset-2 col-sm-10">--}}
    {{-- <button type="submit" class="btn btn-lg btn-primary">Update Products</button>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- </form>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- </div>--}}
    {{-- <!-- /# card -->--}}
    {{-- </div>--}}
    {{-- <!-- /# column -->--}}
    {{-- </div>--}}


    @endsection