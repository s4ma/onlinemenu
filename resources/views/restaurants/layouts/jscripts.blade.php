@yield("script_variants")
<script>
                        var product_variant = {
                            add_new: (element_variants_list, value = "") => {
                                let div = document.createElement('div');
                                element_variants_list.appendChild(div);

                                div.outerHTML =
                                    `<div class="a_variant">
                                        <input type="text" value="${value}" pattern = "[a-zA-Z0-9]+" title="Letters from a to z" required name="variant_name[]" class="variant_name" placeholder="Variant name">
                                        <input type="text" pattern = "[a-zA-Z0-9]+" title="Letters from a to z" required name="variant_title[]" class="variant_title" placeholder="Title">
                                        <input type="number" required name="variant_price[]" class="variant_price" placeholder="Additional price">
                                        <select class="select_mode_view">
                                            <option selected value="1">Dropdown</option>
                                            <option value="2">Checkboxes</option>
                                        </select>
                                        <span class="btn add_title" onclick="product_variant.add_title(this);" style="background-color: #fff;">Add title</span>
                                        <span class="btn btnClose_" onclick="product_variant.delete(this)"></span>
                                    </div>`;
                            },
                            add_title: this_element => {
                                let div = document.createElement('div');
                                this_element.parentNode.appendChild(div);
                                div.outerHTML =
                                    `<div class="title_list">
                                        <input type="text" pattern = "[a-zA-Z0-9]+" title="Letters from a to z" required name="variant_title[]" class="variant_title" placeholder="Title">
                                        <input type="number" required name="variant_price[]" class="variant_price" placeholder="Additional price">
                                        <span class="btn btnClose_" onclick="product_variant.delete(this)"></span>
                                     </div>
                                    `;
                            },
                            delete: this_element => {
                                this_element.parentNode.outerHTML = "";
                            },
                            active: element_add_variants => {
                                if (cbxProductVariants.checked == true) {
                                    element_add_variants.style.display = "block";
                                } else {
                                    element_add_variants.style.display = "none";
                                }
                            },
                            recover: (variants_list, add_variants, cbxProductVariants) => {
                                if (variants_map) {

                                    cbxProductVariants.checked = true;
                                    product_variant.active(add_variants);

                                    var i = 0;

                                    variants_map.get("variant_name").forEach((element,index) => {

                                        product_variant.add_new(variants_list, element.slice(0,-2));
                                        
                                        let end = i + parseInt(element.slice(-1));
                                        console.log("start "+ i + " end " +end);

                                        for(i ; i < end-1 ; i++){
                                            product_variant.add_title(document.querySelectorAll(".add_title")[index]);
                                        }


/*                                         let a_variant = document.querySelectorAll(".a_variant")[index];
                                        let variant_title = a_variant.querySelectorAll(".variant_title");

                                        variant_title.forEach(element=>{
                                            element.value = variants_map.get("variant_title")[end];
                                        }); */
                                        
                                        i = end;
                                    });
                                        let variant_title = document.querySelectorAll(".variant_title");
                                        let variant_price = document.querySelectorAll(".variant_price");
                                        let select_mode_view = document.querySelectorAll(".select_mode_view");

                                        variant_title.forEach((element, c)=>{
                                            element.value = variants_map.get("variant_title")[c];
                                        });
                                        variant_price.forEach((element, c)=>{
                                            element.value = variants_map.get("variant_price")[c];
                                        });
                                        select_mode_view.forEach((element, c)=>{
                                            element.value = variants_map.get("dropdown")[c];
                                        });

                                }

                            }
                        }





                        var cbxProductVariants = document.querySelector("#cbxProductVariants");
                        var add_variants = document.querySelector(".add_variants");
                        var variants_list = document.querySelector(".variants_list");
                        var form_page = document.getElementById("form_page");

                        cbxProductVariants.addEventListener("click", () => {
                            product_variant.active(add_variants);
                        });

                        btnAddVariant.addEventListener('click', () => {
                            product_variant.add_new(variants_list);
                        });


                        form_page.onsubmit = () => {
                            let variant_name = document.querySelectorAll(".variant_name");

                            variant_name.forEach(element => {
                                let titles = element.parentNode.querySelectorAll('div').length;
                                titles = (titles + 1);

                                element.value += `:${titles}`;


                            });

                        }

                        product_variant.recover(variants_list, add_variants, cbxProductVariants);
                    </script>