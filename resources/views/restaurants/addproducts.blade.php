@extends("restaurants.layouts.restaurantslayout")

@section("restaurantcontant")

<style>
    .add_variants {
        width: 100%;
        height: auto;
        background: #f3f4fa;
        display: none;
        padding: 10px 0px;
    }

    .a_variant {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        margin: 8px 0px;
    }


    .a_variant input {
        border: 1px solid #ccc;
        border-radius: 4px;
        margin: 4px 10px;
    }

    .a_variant span {
        margin: 4px 10px;
    }

    #btnAddVariant {
        text-align: center;
        width: 150px;
        background: #fbb140;
        color: #fff;
    }

    .center_button_variant {
        display: flex;
        justify-content: center;
        margin: 10px 0;
    }

    .btnClose_ {
        background-image: url(http://127.0.0.1:8000/assets/images/delete.svg);
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
        color: transparent;
        padding: 1em;
    }

    .btn.btnClose_:hover {
        box-shadow: none;
    }

    .title_list {
        width: 100%;
        height: auto;
        justify-content: center;
        margin: 10px 0 10px 0;
        display: flex;
    }

    .title_list input {
        padding: 10px 3px;
    }

    select.select_mode_view {
        height: 43px;
        margin-top: 4px;
        border: none;
        background: #fff;
        border-radius: 5px;
        color: #525f7f;
    }
</style>
<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
            <h3 class="mb-0">Add Products</h3>
            @if(session()->has("MSG"))
            <div class="alert alert-{{session()->get("TYPE")}}">
                <strong> <a>{{session()->get("MSG")}}</a></strong>
            </div>
            @endif
            @if($errors->any()) @include('admin.admin_layout.form_error') @endif
        </div>
        <!-- Card body -->
        <div class="card-body">
            <form method="post" id="form_page" action="{{route('store_admin.addproducts')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <!-- Form groups used in grid -->
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols1Input">Image</label>




                            <div class="custom-file">
                                <input name="image_url" class="file-name input-flat ui-autocomplete-input" type="file" readonly="readonly" placeholder="Browses photo" autocomplete="off">


                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols2Input">Product Name</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols2Input">Price</label>
                            <input type="number" name="price" class="form-control" required>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group" style="display: none;">
                            <label class="form-control-label" for="example3cols2Input">Cooking Time</label>
                            <input type="number" value="1" name="cooking_time" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label style="position: relative;top:40px;user-select: none;">
                                <input style="transform: scale(1.5);margin-right: 6px" type="checkbox" name="product_variant" id="cbxProductVariants">
                                Product variants
                            </label>
                        </div>
                    </div>
                    <div class="add_variants">
                        <div class="variants_list">
                            <!-- List of variants by js -->
                            
                        </div>
                        <div class="center_button_variant">
                            <span class="btn" id="btnAddVariant" style="text-align: center;">Add variant</span>
                        </div>
                    </div>
                    <script>
                        var cbxProductVariants = document.querySelector("#cbxProductVariants");
                        var add_variants = document.querySelector(".add_variants");
                        var variants_list = document.querySelector(".variants_list");
                        var form_page = document.getElementById("form_page");

                        cbxProductVariants.addEventListener("click", () => {
                            if (cbxProductVariants.checked == true) {
                                add_variants.style.display = "block";
                            } else {
                                add_variants.style.display = "none";
                            }
                        });
                        btnAddVariant.addEventListener('click', () => {
                            let div = document.createElement('div');
                            variants_list.appendChild(div);

                            div.outerHTML =
                                `<div class="a_variant">
                                    <input type="text" pattern = "[a-zA-Z0-9 ]+" title="Letters from a to z" required name="variant_name[]" class="variant_name" placeholder="Variant name">
                                    <input type="text" pattern = "[a-zA-Z0-9 ]+" title="Letters from a to z" required name="variant_title[]" class="variant_title" placeholder="Title">
                                    <input type="number" required name="variant_price[]" class="variant_price" placeholder="Additional price">
                                    <select class="select_mode_view" name="dropdown[]">
                                        <option selected value="1">Dropdown</option>
                                        <option value="2">Checkboxes</option>
                                    </select>
                                    <span class="btn" onclick="add_title(this);" style="background-color: #fff;">Add title</span>
                                    <span class="btn btnClose_" onclick="delete_this(this)"></span>
                                </div>`;
                        });



                        //function for delete a variant
                        function delete_this(this_element) {
                            this_element.parentNode.outerHTML = "";
                        }
                        //function for add another title for a variant
                        function add_title(this_element) {

                            let div = document.createElement('div');
                            this_element.parentNode.appendChild(div);
                            div.outerHTML =
                                `   <div class="title_list">
                                        <input type="text" pattern = "[a-zA-Z0-9 ]+" title="Letters from a to z" required name="variant_title[]" class="variant_title" placeholder="Title">
                                        <input type="number" required name="variant_price[]" class="variant_price" placeholder="Additional price">
                                        <span class="btn btnClose_" onclick="delete_this(this)"></span>
                                    </div>
                                `;

                        }

                        form_page.onsubmit = ()=>{
                            let variant_name = document.querySelectorAll(".variant_name");

                            variant_name.forEach(element=>{
                                let titles = element.parentNode.querySelectorAll('div').length;
                                titles = (titles + 1);

                                element.value += `:${titles}`;

                                
                            });

                        }
                        
                    </script>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Select Category</label>
                            <select class="form-control" name="category_id" required>
                                @foreach($category as $cat)
                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Is Enabled</label>
                            <select class="form-control" name="is_active" required>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Is Recommended</label>
                            <select class="form-control" name="is_recommended" required>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Is Veg</label>
                            <select class="form-control" name="is_veg" required>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Description</label>
                            <textarea class="form-control" name="description" rows="3" required></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>


                </div>

            </form>
        </div>



    </div>



    @endsection