<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$table->table_name}}</title>
    {{-- TOKEN --}}
    <meta name="csrf-token" content="{{csrf_token()}}" />


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">

    <!--  CSS -->
    <link rel="stylesheet" href={{asset('new/css/argon.css?v=1.1.0')}} type="text/css">

    <!-- Page plugins -->
    <link rel="stylesheet" href={{asset('new/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}>
    <link rel="stylesheet" href={{asset('new/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}>
    <link rel="stylesheet" href={{asset('new/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}>

    <!-- Page plugins -->
    <link rel="stylesheet" href={{asset('new/vendor/select2/dist/css/select2.min.css')}}>
    <link rel="stylesheet" href={{asset('new/vendor/quill/dist/quill.core.css')}}>

    <link rel="stylesheet" href={{asset('new/css/chef.css')}} type="text/css">


    <link rel="stylesheet" href={{asset('new/vendor/nucleo/css/nucleo.css')}} type="text/css">

    <script type="text/javascript">
        var orders = "{{json_encode($individual_orders)}}".replace(/&quot;/g, '"');
        orders = JSON.parse(orders);
        //console.log(orders);

        var map_orders = new Map();
        orders.forEach((element, i) => {
            map_orders.set(i, element);
        });


        var in_cart_orders = "{{json_encode($in_cart_orders)}}".replace(/&quot;/g, '"');
        in_cart_orders = JSON.parse(in_cart_orders);

        var cart = new Map();
        in_cart_orders.forEach((element, i) => {
            cart.set(i, element);
        });

        var ind_orders = new Map();


    </script>
    {{-- ALERTS --}}
    <script src="{{asset('js/alerts.js')}}"></script>
    {{-- ALERTS --}}

</head>
<body>
    <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
        <div class="scrollbar-inner">
            <!-- Brand -->
            <div class="sidenav-header d-flex align-items-center">
                <a class="navbar-brand">
                    <h2>STORE NAME</h2>
                </a>
                <div class="ml-auto">
                    <!-- Sidenav toggler -->
                    <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar-inner">
                <!-- Collapse -->
                <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                    <!-- Nav items -->
                    <ul class="navbar-nav">




                        <li {{Route::currentRouteNamed('store_admin.dashboard')? 'class=nav-item active':null }}>
                            <a class="nav-link" href="{{route('store_admin.dashboard')}}">
                                <i class="ni ni-books text-blue"></i>
                                <span class="nav-link-text">Dashboard</span>
                            </a>
                        </li>




                        <li {{ Request::is("admin/store/orders*") ? 'class=nav-item active':null}}>
                            <a class="nav-link" href="{{route('store_admin.orders')}}">
                                <i class="ni ni-cart text-green"></i>
                                <span class="nav-link-text">Orders</span>
                            </a>
                        </li>



                        {{-- <li {{Route::currentRouteNamed('store_admin.banner')? 'class=nav-item active':null }}>
                        <a class="nav-link" href="{{route('store_admin.banner')}}">
                            <i class="ni ni-bullet-list-67 text-yellow"></i>
                            <span class="nav-link-text">Promo Banner</span>
                        </a>
                        </li> --}}


                        <li class="nav-item">
                            <a class="nav-link" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
                                <i class="ni ni-box-2 text-orange"></i>
                                <span class="nav-link-text">Inventory</span>
                            </a>
                            <div class="collapse" id="navbar-examples">
                                <ul class="nav nav-sm flex-column">
                                    <li {{ Request::is("admin/store/categories*") ? 'class=nav-item active':null}} class="nav-item">
                                        <a href="{{route('store_admin.categories')}}" class="nav-link">Category</a>
                                    </li>
                                    <li {{ Request::is("admin/store/products*") ? 'class=nav-item active':null}} class="nav-item">
                                        <a href="{{route('store_admin.products')}}" class="nav-link">Products</a>
                                    </li>

                                </ul>
                            </div>
                        </li>


                        <li {{Route::currentRouteNamed('store_admin.all_tables')? 'class=nav-item active':null }}>
                            <a class="nav-link" href="{{route('store_admin.all_tables')}}">
                                <i class="ni ni-bullet-list-67 text-info"></i>
                                <span class="nav-link-text">Tables</span>
                            </a>
                        </li>

                        {{-- <li class="nav-item">
                            <a class="nav-link" href="#">
                                <i class="ni ni-cloud-download-95 text-green"></i>
                                <span class="nav-link-text"> Print Qr-Code</span>
                            </a>
                        </li> --}}
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('store_admin.subscription_plans')}}">
                                <i class="ni ni-spaceship text-flat-darker"></i>
                                <span class="nav-link-text"> Subscription Plans</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{route('store_admin.settings')}}">
                                <i class="ni ni-settings text-blue"></i>
                                <span class="nav-link-text"> Settings</span>
                            </a>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="ni ni-lock-circle-open text-pink"></i>
                                <span class="nav-link-text"> Logout</span>
                            </a>
                        </li>
                        <form id="logout-form" action="{{route('store.logout')}}" method="POST" style="display: none;">
                            {{csrf_field()}}
                        </form>
                    </ul>




                    </ul>
                    <!-- Divider -->



                </div>
            </div>
        </div>
    </nav>

    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        <nav class="navbar navbar-top navbar-expand navbar-dark bg-gradient-warning border-bottom">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Search form -->

                    <!-- Navbar links -->
                    <ul class="navbar-nav align-items-center ml-md-auto">
                        <li class="nav-item d-xl-none">
                            <!-- Sidenav toggler -->
                            <div class="pr-3 sidenav-toggler sidenav-toggler-light" data-action="sidenav-pin" data-target="#sidenav-main">
                                <div class="sidenav-toggler-inner">
                                    <i class="sidenav-toggler-line"></i>
                                    <i class="sidenav-toggler-line"></i>
                                    <i class="sidenav-toggler-line"></i>
                                </div>
                            </div>
                        </li>


                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ni ni-ungroup"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-dark bg-default dropdown-menu-right">
                                <div class="row shortcuts px-4">
                                    <a href="" class="col-4 shortcut-item">
                                        <span class="shortcut-media avatar rounded-circle bg-gradient-red">
                                            <i class="ni ni-album-2"></i>
                                        </span>
                                        <small>Sliders</small>
                                    </a>
                                    <a href="" class="col-4 shortcut-item">
                                        <span class="shortcut-media avatar rounded-circle bg-gradient-orange">
                                            <i class="ni ni-bag-17"></i>
                                        </span>
                                        <small>Add Store</small>
                                    </a>
                                    <a href="" class="col-4 shortcut-item">
                                        <span class="shortcut-media avatar rounded-circle bg-gradient-info">
                                            <i class="ni ni-basket"></i>
                                        </span>
                                        <small>View Stores</small>
                                    </a>



                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav align-items-center ml-auto ml-md-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="media align-items-center">
                                    <span class="avatar avatar-sm rounded-circle">
                                        <img alt="Image placeholder" src={{asset("assets/images/avatar/1.jpg")}}>
                                    </span>
                                    <div class="media-body ml-2 d-none d-lg-block">
                                        <span class="mb-0 text-sm  font-weight-bold" style="color: #000000;">STORE NAME</span>
                                    </div>
                                </div>
                            </a>

                        </li>
                    </ul>
                </div>
            </div>
        </nav>




        <style>
            button.btn {
                padding: 4px;
                margin: 4px 0px;
                white-space: nowrap;
            }

            .table td,
            .table th {
                font-size: .8125rem;
                white-space: unset;
            }

            .buttons {
                display: flex;
            }

            .CONT {
                margin-top: 40px;
                background: #fff;
                padding: 16px 2px;
                border-radius: 10px;
                box-shadow: 0px 4px 14px 0px #0000001f;
            }

            thead {
                background-color: #f6f9fc;
            }

            .h2 {
                margin-left: 8px;
            }

            .content-right {
                display: flex;
                justify-content: flex-end;
                padding: 5px 10px;
            }

        </style>


        <div class="container CONT">
            <h2 class="h2">{{$table->table_name}}</h2>
            <table class="table table-hover TABLA">
                <thead>
                    <tr>
                        <th>PRODUCT NAME WITH VARIATION </th>
                        <th>Price item</th>
                        <th>Quantity</th>
                        <th>Total price</th>
                        <th>Accept / Reject</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="first_table">
                </tbody>
            </table>
        </div>

        <div class="container CONT">
            <h2 class="h2">Cart</h2>
            <table class="table table-hover TABLA">
                <thead>
                    <tr>
                        <th>PRODUCT NAME WITH VARIATION </th>
                        <th>Price item</th>
                        <th>Quantity</th>
                        <th>Total price</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody id="second_table">

                </tbody>
            </table>
            <div class="content-right">
                <button class="btn btn-info" onclick="">Pay Cash</button>
                <button class="btn btn-info">Pay Credit Card</button>
            </div>
        </div>


        {{-- JQUERY --}}
        <script src={{asset("new/vendor/jquery/dist/jquery.min.js")}}></script>
        {{-- JQUERY --}}



        {{-- MY SCRIPT --}}
        <script src="{{asset('js/v_table.js')}}"></script>
        {{-- MY SCRIPT --}}



        <div>

            <script src={{asset("new/vendor/bootstrap/dist/js/bootstrap.bundle.min.js")}}></script>
            <script src={{asset("new/vendor/js-cookie/js.cookie.js")}}></script>
            <script src={{asset("new/vendor/jquery.scrollbar/jquery.scrollbar.min.js")}}></script>
            <script src={{asset("new/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js")}}></script>
            <script src={{asset("new/vendor/chart.js/dist/Chart.min.js")}}></script>
            <script src={{asset("new/vendor/chart.js/dist/Chart.extension.js")}}></script>
            <script src={{asset("new/vendor/jvectormap-next/jquery-jvectormap.min.js")}}></script>
            <script src={{asset("new/js/vendor/jvectormap/jquery-jvectormap-world-mill.js")}}></script>
            <script src={{asset("new/js/argon.js?v=1.1.0")}}></script>





            <script src={{asset("new/vendor/select2/dist/js/select2.min.js")}}></script>
            <script src={{asset("new/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")}}></script>
            <script src={{asset("new/vendor/nouislider/distribute/nouislider.min.js")}}></script>
            <script src={{asset("new/vendor/quill/dist/quill.min.js")}}></script>
            <script src={{asset("new/vendor/dropzone/dist/min/dropzone.min.js")}}></script>
            <script src={{asset("new/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js")}}></script>
            <script src={{asset("assets/js/printthis.js")}}></script>



            <script>
                $('#printButton').on('click', function() {
                    $('#printThis').printThis();
                })
                //on single click, accpet order and disable button
                $('body').on("click", ".acceptOrderBtn", function(e) {
                    $(this).addClass('pointer-none');
                });
                //on Single click donot cancel order
                $('body').on("click", ".cancelOrderBtn", function(e) {
                    return false;
                });
                //cancel order on double click
                $('body').on("dblclick", ".cancelOrderBtn", function(e) {
                    $(this).addClass('pointer-none');
                    window.location = this.href;
                    return false;
                });

            </script>
            <script>
                $('#modal-notification').modal('show')

            </script>
            
            
        </div>

</body>
</html>
