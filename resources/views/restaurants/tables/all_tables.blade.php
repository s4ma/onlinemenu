@extends("restaurants.layouts.restaurantslayout")

@section("restaurantcontant")

<style>
    .btn_qr_code {
        position: absolute;
        right: 0;
    }

    .a_buttons {
        display: flex;
        flex-wrap: wrap;
    }
    .CARD_TABLE{
        cursor: pointer;
    }

</style>

<div class="container-fluid mt--6">

    <div class="border-0">
        <div class="row">
            <div class="col-6">

            </div>
            <div class="col-6 text-right">
                <button onclick="event.preventDefault(); document.getElementById('add_new').submit();" class="btn btn-sm btn-primary btn-round btn-icon" data-toggle="tooltip" data-original-title="Add Tables">
                    <span class="btn-inner--icon"><i class="fas fa-user-edit"></i></span>
                    <span class="btn-inner--text">Add Tables</span>
                </button>
                <form action="{{route('store_admin.add_tables')}}" method="get" id="add_new"></form>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="row row-example">
            <?php 
                $protocol   = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://'; 
                $host       = $_SERVER["HTTP_HOST"];
                $link       = $protocol . $host;
            ?>

            @foreach($tables as $data)
            <div class="col-sm-3">
                <div class="card CARD_TABLE" onclick="card_table_url(this, '{{$data->id}}', event);" data-name="table_{{$data->table_name}}" style="border: 2px dashed #001354;">
                    <div class="card-body store_id" data-id="{{$data->store_id}}">
                        <!-- List group -->
                        <ul class="list-group list-group-flush list my--3">
                            <li class="list-group-item px-0">
                                <a href="{{$link}}/qrtablecode/{{$data->store_id}}/{{$data->id}}" onclick="card_table_url(this, 'no', event);" target="_blank" class="btn btn-sm btn-primary btn_qr_code">QR</a>
                                <div class="row align-items-center">

                                    <div class="col">
                                        <h4>
                                            <b>Table No : {{$data->table_name}}</b>
                                        </h4>
                                        <br>
                                        <div class="a_buttons">
                                            <a href="{{route('store_admin.edit_table',$data->id)}}" onclick="card_table_url(this, 'no', event);" class="btn btn-sm btn-primary">Edit</a>
                                            {{-- <a href="" class="btn btn-sm btn-primary">ver</a> --}}
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <label class="custom-toggle">
                                            <input type="checkbox" disabled {{$data->is_active ==1?"checked":NULL}}>
                                            <span class="custom-toggle-slider rounded-circle" data-label-off="Off" data-label-on="On"></span>
                                        </label>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

            @endforeach
        </div>
    </div>
    {{-- MY SCRIPTs --}}

    <script>
        function card_table_url(element, data_id, event){
            console.log(data_id);
            if(data_id == 'no'){
                event.stopPropagation();
            }else{
                document.location.href = `/admin/store/thetable/${data_id}`;
            }

        }
    </script>
    <script src={{asset("new/vendor/jquery/dist/jquery.min.js")}}></script>

    <script src="{{asset('js/table_alert.js')}}"></script>

    {{-- MY SCRIPTs --}}




















</div>




@endsection
