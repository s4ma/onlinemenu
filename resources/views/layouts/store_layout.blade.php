<?php
    $protocol   = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
    $host       = $_SERVER["HTTP_HOST"];
    $page_dir   = $protocol . $host . "/";

    session_start();

    $_SESSION['store_name'] = $store->store_name;
    $_SESSION['table_name'] = $table->table_name;
    
    $_SESSION['id_table'] = $id_table;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title_page')</title>
</head>
<script>
    const currency = '{{$store->currency}}';
</script>
<style>
    body {
        background: #f0f2f5;
        margin-bottom: 60px;
    }

    * {
        margin: 0;
        padding: 0;
    }

    h1.store_name {
        color: #28a745;
        font-weight: 700;
        font-size: 1.6rem;
        font-family: sans-serif;
        margin: 16px;
    }

    header.header {
        background-color: #f0f2f5;
        padding: 1px 0px;
        border-bottom: 1px solid #dee2e6;
    }

    .icon-search {
        width: 20px;
        height: 20px;
        background-image: url(http://127.0.0.1:8000/svg/icon-buscar-green.svg);
        background-repeat: no-repeat;
        background-size: 15px;
        background-position: center;
        margin-right: 6px;
    }

    form.form_search {
        display: flex;
        margin: 10px;
        padding: 8px 5px;
        box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, .075);
        border-radius: 8px;
        background-color: #fff;
    }

    input#inpSearchProducts {
        border: none;
        width: 86%;
    }

    .categories {
        background-color: #f0f2f5;
        padding: 5px 0;
    }

    h4.cat_title {
        font-size: 1.1em;
        font-family: sans-serif;
        color: #000000a6;
        margin: 10px;
    }

    .items_categories {
        display: flex;
        flex-wrap: wrap;
        padding: 2px 15px 12px;
    }

    .item_container {
        background-color: #fff;
        padding: 16px 10px;
        border-radius: 8px;
        margin: 0 8px;
        box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, .075);
    }

    .item_img {
        display: flex;
        justify-content: center;
    }

    img.cat_img {
        width: 50px;
        height: 50px;
        border-radius: 7px
    }

    .item_title {
        font-family: sans-serif;
        font-size: 12px;
        display: flex;
        justify-content: center;
        color: #6c757d;
        margin: 7px 0 0 0;
    }

    footer {
        background: #fff;
        position: fixed;
        width: 100%;
        bottom: 0;
        display: grid;
        box-shadow: -2px -2px 10px rgba(0, 0, 0, .075);
        grid-template-columns: 1fr 1fr 1fr;
        padding: 2px 0px 7px;
    }

    .item_footer_img {
        width: 30px;
        height: 30px;
        background-size: 18px;
        background-repeat: no-repeat;
        background-position: center;
    }

    .item_footer {
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .item_footer strong {
        font-family: sans-serif;
        font-size: 13px;
        color: #4a4f55;
    }

    h4.table_name {
        text-align: center;
        font-family: sans-serif;
        color: #484848;
    }

    .a_product {
        background: #fff;
        width: 300px;
        max-width: 360px;
        margin: 16px auto;
        border-radius: 10px;
        box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, .075);
    }

    .top_card {
        padding: 8px;
        text-align: center;
    }

    .top_card img {
        width: 90%;
        border-radius: 8px;
        box-shadow: 0px 2px 2px #75757566;
    }

    .center_card {
        display: flex;
        flex-direction: column;
        padding: 0 1.4rem 1rem;
        font-family: 'Ubuntu', sans-serif;
    }

    .center_card strong {
        color: #28a745;
        font-weight: 700;
        margin-bottom: .25rem;
        font-size: 1.1em;
    }

    .center_card span {
        color: #6c757d;
        font-size: .9em;
    }

    .bottom_card {
        display: flex;
        justify-content: space-between;
        padding: 0 1.4em;
        font-family: 'Ubuntu', sans-serif;
        padding-bottom: 1rem;
        align-items: center;
    }

    .btn_add {
        color: #ff6000;
        box-shadow: 0px 1px 4px 0px #969696cc;
        width: 25px;
        height: 25px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        background: #fff;
    }

    .add_to_cart {
        background: #e6e6eaa3;
        width: 90px;
        display: flex;
        justify-content: center;
        border-radius: 15px;
        height: 32px;
        align-items: center;
    }

    .products_recommend h3 {
        font-family: 'ubuntu', sans-serif;
        margin-left: 35px;
        width: 100%;
    }

    .products_recommend {
        display: flex;
        flex-wrap: wrap;
    }

    .product_cat {
        background-color: #fff;
        width: 140px;
        border-radius: 8px;
        box-shadow: 0px 1px 7px #00000045;
        height: 210px;
        margin: 20px 10px;
        padding-top: 10px;
    }

    .product_top {
        width: 82%;
        height: 140px;
        margin: 0 auto;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
    }

    .poroduct_bottom {
        padding: 10px;
        font-family: 'Ubuntu', sans-serif;
    }

    strong.prod_name {
        margin-left: 3px;
    }

    .price_cart {
        display: flex;
        width: 100%;
        justify-content: space-between;
        align-items: center;
        position: relative;
        top: 6px;
    }

    span.prod_price {
        color: #28a745;
    }

    section.a_category {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
    }

    section.a_category h3 {
        width: 100%;
    }

    section.a_category h3 {
        font-family: 'Ubuntu', sans-serif;
        margin: 6px 12px;
    }

</style>
<body>
    {{-- HEADER SECTION --}}
    <header class="header">
        <h1 class="store_name">@yield('store_name')</h1>
        <h4 class="table_name"> {{ $table->table_name }} </h4>
    </header>

    {{-- CATEGORIES SECTION --}}
    <div class="categories">
        <div class="part_1">
            <form class="form_search">
                <div class="icon-search"></div>
                <input type="search" id="inpSearchProducts" name="search" placeholder="Search for products" />
            </form>
        </div>
        <div class="part_2">
            <h4 class="cat_title">Categories</h4>
            <div class="items_categories">
                @foreach($categories as $item)

                <div class="item_container" onclick="document.location.href='#cat_{{$item->id}}';">
                    <div class="item_img">
                        <?php $url = $_SERVER['HTTP_HOST'] . "/" . $item->image_url ?>
                        <img src="{{$protocol}}{{ $url }}" class="cat_img" />
                    </div>
                    <div class="item_title">
                        <p> {{ $item->name }} </p>
                    </div>
                </div>



                @endforeach
            </div>
        </div>
    </div>
    {{-- RECOMMEND FOR YOU SECTION --}}
    <div class="products_recommend">
        {{-- here are the recommended products, printed by JS --}}
        <h3>Recommended for you</h3>
        @foreach($products as $item)
        <div class="a_product" onclick="document.location.href='{{$page_dir}}storeproduct/{{$store->store_name}}/{{$item->id}}'">
            <div class="top_card">
                <img src="{{$page_dir}}{{$item->image_url}}" alt="Product image" />
            </div>
            <div class="center_card">
                <strong>{{$item->name}}</strong>
                <span>{{$item->description}}</span>
            </div>
            <div class="bottom_card">
                <div class="price">
                    <strong>{{$item->price}} {{$store->currency}}</strong>
                </div>
                <div class="add_to_cart">
                    <div class="btn_add">+</div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    {{-- PRODUCTS CATEGORIES --}}
    <dic class="products_categories">
        @foreach($categories as $cat)
        <section class="a_category" id="cat_{{$cat->id}}">
            <h3>{{$cat->name}}</h3>
            @foreach($products as $prod )
            @if($prod->category_id == $cat->id)
            <div class="product_cat" onclick="goTo(`/storeproduct/{{$store->store_name}}/{{$prod->id}}`)">
                <div class="product_top" style="background-image: url({{$page_dir}}{{$prod->image_url}});"></div>
                <div class="poroduct_bottom">
                    <strong class="prod_name">{{$prod->name}}</strong>
                    <div class="price_cart">
                        <span class="prod_price">{{$store->currency}} {{$prod->price}}</span>

                        {{-- THIS IS FOR END, REPAIR THE ONCLICK FUNCTION OF THE NEXT ELEMENT --}}
                        <div class="btn_add" onclick="addToCart(`<?php echo json_encode($prod) ?>`)">+</div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </section>


        @endforeach

        </div>






        {{-- FOOTER SECTION --}}
        <footer>
            <div class="item_footer" >
                <div class="item_footer_img" style="background-image: url({{asset('svg/icon-buy-green.svg')}})"></div>
                <strong>Shop</strong>
            </div>
            <div class="item_footer" onclick="goTo(`/shoppingcart/{{$id_store}}/{{$id_table}}`);">
                <div class="item_footer_img" style="background-image: url({{asset('svg/icon-cart-gray.svg')}})"></div>
                <strong>Cart</strong>
            </div>
            <div class="item_footer" onclick="goTo(`/table/{{$id_store}}/{{$id_table}}/orders`)">
                <div class="item_footer_img" style="background-image: url({{asset('svg/icon-order-gray.svg')}})"></div>
                <strong>My Order</strong>
            </div>
        </footer>
        <script src="{{asset('js/product.js')}}"></script>
</body>
</html>








{{-- <img src="{{ asset('svg/icon-qr-code.svg') }}"> --}}
