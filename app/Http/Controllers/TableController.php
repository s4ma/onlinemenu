<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Echo_;
use function GuzzleHttp\json_decode;

class TableController extends Controller
{
    public function index($id_store, $id_table)
    {

        $store = DB::table('stores')->where('id', '=', $id_store)->first();
        $categories = DB::table('categories')->where('store_id', '=', $id_store)->get();
        $products = DB::table('products')->where('store_id', '=', $id_store)->take(10)->get();
        $table = DB::table('tables')->where('id', '=', $id_table)->first();



        return view("table", [
            'id_store'      => $id_store,
            'id_table'      => $id_table,
            'store'         => $store,
            'categories'    => $categories,
            'products'      => $products,
            'table'         => $table
        ]);
    }
    public function orders($id_store, $id_table)
    {
        $table = DB::table('tables')->select('table_name')->where('id', '=', $id_table)->first();
        $orders = DB::table('orders')->where('store_id', '=', $id_store)
            ->where('table_no', '=', $table->{'table_name'})
            ->orWhere('status', '=', 1)
            ->orWhere('status', '=', 2)
            ->orWhere('status', '=', 3)
            ->get();

        /* echo $orders;
        die(); */

        return view('orders', [
            'id_table'      => $id_table,
            'id_store'      => $id_store,
            'table_name'    => $table->{'table_name'},
            'orders'        => $orders
        ]);
    }

    public function product($store_name, $id_product)
    {

        $the_product = DB::table('products')->where('id', '=', $id_product)->first();

        $store = DB::table('stores')->where('store_name', '=', $store_name)->first();


        $variants = DB::table('products_variants')->where('id_product', '=', $id_product)->where('visibility', '=', 1)->first();


        if (!isset($store->id) || !isset($the_product->id)) {
            die("<center><h2 style='font-family: sans-serif;'>ERROR 404</h2></center>");
        }

        session_start();
        if (isset($_SESSION['id_table'])) {
            $id_table = $_SESSION['id_table'];
        } else {

            $id_table = 0;
        }
        /* if(!isset($the_product->id)){
            die("<center><h2 style='font-family: sans-serif;'>ERROR 404</h2></center>");
        } */

        $pass = [
            'store_name'    => $store_name,
            'id_product'    => $id_product,
            'the_product'   => $the_product,
            'variants'      => $variants,
            'id_store'      => $store->id,
            'id_table'      => $id_table,
            'currency'      => $store->currency
        ];

        return view("storeproduct", $pass);
    }
    public function cart(Request $request)
    {

        $order = $request['order'];
        $id_store = $request['id_store'];
        $store_name = $request['store_name'];
        $comment = $request['comment'];
        $order_details = json_decode($request['order_details']);
        $table_name = $request['table_name'];

        $subtotal = $request['subtotal'];

        $order_final = "";

        foreach ($order_details as $key) {

            $order_final .= "\n |  " . $key->{'order'};
        }

        $ouid = "ODR-" . time();

        DB::select("INSERT INTO `orders` (
            `order_unique_id`,
            `store_id`,
            `table_no`,
            `sub_total`,
            `total`,
            `status`,
            `created_at`, 
            `updated_at`,
            `comments`,
            `details`
            ) VALUES (
            '$ouid',
             $id_store,
            '$table_name',
             $subtotal,
             $subtotal,
             1,
             CURRENT_DATE,
             CURRENT_DATE,
             '$comment',
             '$order_final'
            ) ");


        $latest = DB::table('orders')->orderBy('id', 'desc')->first();



        foreach ($order_details as $key) {
            $id = $latest->id;
            $name = $key->{'name'};
            $price = $key->{'price'};
            $quantity = $key->{'quantity'};
            $order = $key->{'order'};

            DB::select("INSERT INTO `order_details`(
                `order_id`,
                `name`,
                `price`,
                `quantity`,
                `order_variants`,
                `table_name`,
                `created_at`, 
                `updated_at`,
                `status`,
                `paid`,
                `in_cart`              

                ) VALUES (
                    $id,
                    '$name',
                    $price,
                    $quantity,
                    '$order',
                    '$table_name',
                    CURRENT_DATE,
                    CURRENT_DATE,
                    1,
                    0,
                    0
                )");


            /*             $con = DB::table('order_details')->insert([
                'order_id'      => $latest->id,
                'name'          => $key->{'name'},
                'price'         => $key->{'price'},
                'quantity'      => $key->{'quantity'},
                'order_variants'=> $key->{'order'},
                'table_name'    => $table_name,
                'status'        => 1,
                'paid'          => 0,
                'in_cart'       => 0
            ]); */
        }
        DB::update("UPDATE `tables` SET `in_use` = 1 WHERE `tables`.`table_name` = '$table_name'");
    }

    public function shoppingcart($id_store, $id_table)
    {

        $store = DB::table('stores')->where('id', '=', $id_store)->first();


        return view('shoppingcart', [
            'id_store'  => $id_store,
            'id_table'  => $id_table,
            'currency'     => $store->currency
        ]);
    }

    public function thetable($id_table)
    {

        $table = DB::table('tables')->where('id', '=', $id_table)->first();
        $table_name = $table->table_name;

        $individual_orders  = DB::select("SELECT * FROM `order_details` WHERE `table_name` = '$table_name' AND (`status` = 1 OR `status` = 2) AND `quantity` != `in_cart`");
        $in_cart_orders     = DB::select("SELECT * FROM `order_details` WHERE `table_name` = '$table_name' AND (`status` = 1 OR `status` = 2) AND `quantity` = `in_cart`");



        if (!isset($table->id)) {
            die("<center><h2 style='font-family: sans-serif;'>ERROR 404</h2></center>");
        }

        return view('restaurants.tables.v_table', [
            'table' => $table,
            'individual_orders' => $individual_orders,
            'in_cart_orders'    => $in_cart_orders
        ]);
    }
    public function DB_add_to_cart(Request $request)
    {

        $order_id =  $request['order_id'];
        $in_cart = $request['in_cart'];
        DB::update("UPDATE `order_details` SET `status` = 2,
                                               `in_cart` = `in_cart` + $in_cart 
                                               WHERE `order_details`.`id` = '$order_id'");
    }
    public function DB_delete_order(Request $request)
    {

        $id_order = $request['order_id'];

        DB::update("UPDATE `order_details` SET `quantity` = `in_cart` WHERE `order_details`.`id` = '$id_order'");
    }

    public function table_alert(Request $request)
    {
        $store_id = $request['store_id'];

        $select = DB::select("SELECT `table_name`, `in_use` FROM `tables` WHERE `store_id` = $store_id");

        echo json_encode($select);
    }

    public function paidCart(Request $request)
    {

        $cart = json_decode($request['cart']);

        foreach ($cart as $order) {

            $quantity = $order->quantity;
            $id = $order->id;

            $q = DB::update("UPDATE `order_details` 
                                        SET `paid` = `paid` + $quantity, 
                                            `updated_at` = CURRENT_DATE
                                         WHERE `id` = $id");
        }
        if ($q) {
            echo "1";
        } else {
            echo "0";
        }
    }

    public function reject_order(Request $request)
    {

        $id_order = $request['order_id'];

        DB::update("UPDATE `order_details` SET `status` = 4, `updated_at` = CURRENT_DATE WHERE `order_details`.`id` = '$id_order'");
    }

    public function daily_report(Request $request)
    {

        $store_id = $request['store_id'];

        if (isset($request['fil_date'])) {
            $DATE = $request['fil_date'];
        } else {
            $currdate =  DB::select("SELECT CURRENT_DATE");

            $arr = json_decode(json_encode($currdate), true);
            $DATE = $arr[0]['CURRENT_DATE'];
        }
        $fp = "2020-11-30";

        $join = DB::table('order_details')
            ->join('orders', 'order_details.order_id', '=', 'orders.id')
            ->select('order_details.*', 'orders.store_id')
            ->where('orders.store_id', '=', $store_id)
            ->where('order_details.created_at', '=', $DATE)
            ->where('order_details.status', '=', 6)
            ->get();

        echo $join;
    }

    public function DB_cancel_cart(Request $request){

        $currdate =  DB::select("SELECT CURRENT_DATE");
        $arr = json_decode(json_encode($currdate), true);
        $DATE = $arr[0]['CURRENT_DATE'];

        if (isset($request['quantity'])) {
            $id =  $request['id'];
            $quantity = $request['quantity'];
            DB::statement("UPDATE `order_details` SET
                                                    `in_cart`   = (`in_cart` - $quantity),
                                                    `updated_at`= '$DATE'
                                                 WHERE `id` = $id");
        } else {
            $id =  $request['id'];
            DB::table('order_details')->where('id', '=', $id)
                ->update([
                    'status' => 1,
                    'in_cart' => 0,
                    'updated_at'=> $DATE
                ]);
            echo $id;
        }
    }
}
