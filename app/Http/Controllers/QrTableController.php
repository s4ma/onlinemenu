<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QrTableController extends Controller
{
    public function index($id_store, $id_table)
    {

        $protocol   = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
        $host       = $_SERVER["HTTP_HOST"];
        $url_qr     = $protocol . $host . "/table" . "/" . $id_store . "/" . $id_table;

        $store = DB::table('stores')->where('id', '=', $id_store)->first();
        $table = DB::table('tables')->where('id', '=', $id_table )->first();

        return view("qrtablecode", [
            'url_qr' => $url_qr,
            'store'         => $store,
            'table'         => $table
        ]);
    }
}
