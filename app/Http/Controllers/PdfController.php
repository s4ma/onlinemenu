<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade;

class PdfController extends Controller
{
    public function PDF()
    {
        $pdf = Facade::loadView('invoice');
        return $pdf->stream('invoice.pdf');
    }
}
