<?php

namespace App\Http\Controllers\StoreAdmin;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use mysqli;

class ProductController extends Controller
{
    public function  __construct()
    {
        $this->middleware('auth:store');
    }


    public function addproducts(Request $request){


        $data = request()->validate([
            'name'=>'required',
            'image_url'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'is_active'=>'required',
            'category_id'=>'required',
            'is_veg'=>'',
            'description'=>'',
            'price'=>'required',
            'cooking_time'=>'required',
            'is_recommended'=>'',

            'store_id'=>''
        ]);
        $data['store_id'] = auth()->id();
        if($request->image_url !=NULL) {
            $url = $request->file("image_url")->store('public/stores/product/images/');
            $data['image_url'] = str_replace("public","storage",$url);
        }
        if(Product::create($data))
            if(isset($_POST['product_variant'])){

                if(isset($_POST['variant_name']) && isset($_POST['variant_title']) && isset($_POST['variant_price']) ){
                    $conn = new mysqli("localhost", "root", "", "shopinnstore");
                    $sql = "SELECT `id` FROM `products` ORDER BY id DESC LIMIT 1";
                    $resultado = mysqli_query($conn, $sql);
                    
                    $last_id = mysqli_fetch_assoc($resultado);
 
                    $id = $last_id['id'];
                    $variant_name = json_encode($_POST['variant_name']);
                    $variant_title = json_encode($_POST['variant_title']);
                    $variant_price = json_encode($_POST['variant_price']);
                    $dropdown = json_encode($_POST['dropdown']);
 
                    $sql_insert = "INSERT INTO `products_variants` (`id`, `id_product`, `variant_name`, `variant_title`, `variant_price`, `dropdown`, `visibility`) VALUES (NULL, '$id', '$variant_name', '$variant_title', '$variant_price', '$dropdown' ,'1');";
                    $insert = mysqli_query($conn, $sql_insert);
 
                    if($insert){
                        return back()->with("MSG","Record added successfully")->with("TYPE", "success");
                    }
             }
            }


            return back()->with("MSG","Record added successfully")->with("TYPE", "success");


    }   


    public function edit_products(Request $request,$id){
        $data = request()->validate([
            'name'=>'required',
            'image_url'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'is_active'=>'required',
            'category_id'=>'required',
            'is_veg'=>'',
            'description'=>'',
            'price'=>'required',
            'cooking_time'=>'required',
            'is_recommended'=>'',
        ]);
        if($request->image_url !=NULL) {
            Storage::delete(str_replace("storage","public",Product::find($id)->image_url));
            $url = $request->file("image_url")->store('public/stores/category/images/');
            $data['image_url'] = str_replace("public","storage",$url);
        }
        Product::whereId($id)->update($data);

        if(isset($_POST['variant_name'])){
            $variant_name = json_encode($_POST['variant_name']);
            $variant_title = json_encode($_POST['variant_title']);
            $variant_price = json_encode($_POST['variant_price']);
            $dropdown = json_encode($_POST['dropdown']);
                        
            $conn = new mysqli("localhost", "root", "", "shopinnstore");
            if(isset($_POST['product_variant'])){
                $sql_update = "UPDATE `products_variants` SET `variant_name` = '$variant_name', `variant_title` = '$variant_title', `variant_price` = '$variant_price', `dropdown` = '$dropdown', `visibility` = '1' WHERE `products_variants`.`id_product` = $id;";
            }else{
                $sql_update = "UPDATE `products_variants` SET `variant_name` = '$variant_name', `variant_title` = '$variant_title', `variant_price` = '$variant_price', `dropdown` = '$dropdown', `visibility` = '0' WHERE `products_variants`.`id_product` = $id;";
            }
            
            $update = mysqli_query($conn, $sql_update);
    
    
            if($update){
                return back()->with("MSG", "Record Updated Successfully.")->with("TYPE", "success");    
            }else{
                return back()->with("MSG", "Record Updated Successfully. But there was an error saving the product variants")->with("TYPE", "success");
            }
        }

        return back()->with("MSG", "Record Updated Successfully.")->with("TYPE", "success"); 


    }
    public function delete_product(Request $request)
    {
        if (Storage::delete(str_replace("storage", "public", Product::find($request->id)->image_url))) {
            Product::destroy($request->id);
        }
        return back();

    }


}
